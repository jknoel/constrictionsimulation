# README #

This README describes the basic steps to running a simulation and explains the relevant literature.

### What is constrictionSimulation? ###

This code implements a coarse-grained simulation of an elastic filament bound on axisymmetric, deformable membrane tube. The parameters are chosen to describe dynamin filaments, but can be readily changed. The filament is represented as a chain of beads and the membrane as an axisymmetric stack of cylindrical disks. Thermal fluctuations and lipid flows are accounted for.

### How do I run a simulation? ###

From the root directory of the distribution a test run can be started by: 
~~~~
$> git clone https://jknoel@bitbucket.org/jknoel/constrictionsimulation.git
$> cd constrictionSimulation
$> distroot=$(pwd)
$> java -classpath "$distroot" noel.dynamin.Simulation --config config
~~~~
This run calculates 100 steps starting from the initial condition in the file equil.40.59.xyz, which contains
40 dynamin dimers in a single filament (40 filament beads) wound around a membrane tube with an equilibrium radius
of 20 nm.

All the simulation parameters can be seen in the file config.

#### Some Notes ####
* There is no self-contained user manual. The published manuscripts contain extensive methods, which provide a conceptual guide and implementation details for the forces. A separate description of the non-scientific aspects, e.g. how neighbor searching is done, etc., is not available.
* Reduced units are employed: microseconds (time), nanometers (length), kT (energy). The temperature is taken to be 310 K. Thus, `temperature 1` in the config file means 310 K, and `mmDimer_constantForce 1.3` means a force of 1.3 kT/nm.
* Most of the available parameters are already in the config file. Add `-?` as a command line parameter for a full list. Any parameter can be overridden by including it as a command line parameter.
* Coordinates are dumped to the output file every `outputStep` number of simulation steps in XYZ format. `$> vmd outputfile` will display the trajectory in [VMD](https://www.ks.uiuc.edu/Research/vmd/) (make sure to use .xyz extension for the outputfile). 
* While the code allows for arbitrary number of filaments, treat any number of filaments greater than 1 as untested. Rules for oligomerization and filament breakup are planned additions.
* `viewerOn true` generates a visualization of the current filament/membrane state during the simulation. This is enabled by [JmolViewer](http://jmol.sourceforge.net/download/) within Jmol.jar and requires that Jmol.jar be in your classpath. For example, if Jmol.jar is placed in the distribution directory the run command would be: `$> java -classpath "$distroot:$disroot/Jmol.jar" noel.dynamin.Simulation --config config`

### Literature and citing ###

The elastic parameters of the dynamin filament are calculated in this [paper](http://doi.org/10.1016/j.bpj.2019.09.042). 
The shapes of the filament/membrane system in the absence of motor activity and filament cross-links (i.e. in the absence of GTP)
are also discussed. The results of this paper can be explored by setting `GTP_concentration = 0` in the config file.

Dynamin motor activity for short and long filaments is discussed [here](https://doi.org/10.1101/2020.09.10.289546). 
In this manuscript, the forces exerted by dynamin G-dimers formed between helical turns are determined using a combination of
smFRET and molecular simulations. The collective operation of dynamin motors during in membrane constriction was studied. It was 
found that the motors operate in a parallel and asynchronous way, similar to myosin II in contraction of the muscle. 

* When using `GTP_concentration = 0` the most relevant citaton is:
	* Noel et al. (2019) Polymer-like Model to Study the Dynamics of Dynamin Filaments on Deformable Membrane Tubes. Biophys. J.
		* http://doi.org/10.1016/j.bpj.2019.09.042
* When exploring motor function (i.e. `GTP_concentration > 0`) the relevant citations is:
	* Ganichkin, Noel et al. (2021) Quantification and demonstration of the collective constriction-by-ratchet mechanism in the dynamin molecular motor. PNAS
		* https://doi.org/10.1073/pnas.2101144118
	
### Who are we? ###
The project is headed by and the code was written by [Jeff Noel](http://smog-server.org/noel). [Alexander Mikhailov](http://mikhailov.w3.kanazawa-u.ac.jp) and [Oliver Daumke](https://www.mdc-berlin.de/daumke) were heavily involved in formulating the dynamin description. 

### Acknowledgements ###

* Argument and input file parsing is performed with the ArgParser package written by John Lloyd (https://www.cs.ubc.ca/~lloyd/java/argparser.html)
* Graphical display of the current state is enabled by [JmolViewer](http://jmol.sourceforge.net/download/)
package noel.dynamin;
import java.util.*;

class Membrane {
	Vector<Disk> disks;
	int numDisks;
	double distM;
	double beginZ, endZ;
	double invdistMx2,invdistM2,invdistM3x2,invdistM4;
	//implicit boundary disks stuff
	double dMm,dM2m,dM3m,dM4m; //minus disk
	double dMp,dM2p,dM3p,dM4p; //plus disk
	boolean[] threads = { true, true, true, true };
	int numThreads = 4;
	Thread[] t;
	
	public Membrane(double[] radii, double[] z) {
		numDisks = radii.length;
		disks = new Vector<Disk>();
		for(int i = 0; i < numDisks; i++) {
			disks.add(new Disk(radii[i],z[i]));	
		}
		beginZ = z[0];
		endZ = z[numDisks-1];
		distM = Param.membraneDiskThickness;
		invdistMx2 = 1.0 / (2 * distM);
		invdistM2 = 1.0 / (distM * distM);
		invdistM3x2 = 1.0 / (distM * distM * distM * 2);
		invdistM4 = 1.0 / (distM*distM*distM*distM);
	}
	
	// private class InnerUpdate implements Runnable {
	// 	int threadIndex;
	// 	int start, end;
	// 	Membrane m;
	//
	// 	public InnerUpdate(int start, int end, Membrane m) {
	// 		this.start = start;
	// 		this.end = end;
	// 		this.m = m;
	// 	}
	// 	public void run() {
	// 		double rm1,rm2,r,rp1,rp2;
	// 		for( int i = start; i < end; i++ ) {
	// 			m.disks.elementAt(i).rf = 0;
	// 			m.disks.elementAt(i).zf = 0;
	// 			m.disks.elementAt(i).flowVelocity = 0;
	// 			m.disks.elementAt(i).r2inv = 1.0/(m.disks.elementAt(i).r*m.disks.elementAt(i).r);
	// 			r = m.disks.elementAt(i).r;
	// 			rm1 = m.disks.elementAt(i-1).r;
	// 			rm2 = m.disks.elementAt(i-2).r;
	// 			rp1 = m.disks.elementAt(i+1).r;
	// 			rp2 = m.disks.elementAt(i+2).r;
	// 			//precompute derivatives along the tube, i.e. dM = dR/dz
	// 			m.disks.elementAt(i).dM = (rp1-rm1)*m.invdistMx2;
	// 			m.disks.elementAt(i).dM2 = (rp1-2*r+rm1)*m.invdistM2;
	// 			m.disks.elementAt(i).dM3 = (rp2-2*rp1+2*rm1-rm2)*m.invdistM3x2;
	// 			m.disks.elementAt(i).dM4 = (rp2-4*rp1+6*r-4*rm1+rm2)*m.invdistM4;
	// 		}
	// 		System.out.println("I worked from "+start+" to "+end+" "+(m.numDisks-2));
	// 	}
	// }
	
	public void update() {
		double rm1,rm2,r,rp1,rp2;
		for( int i = 2; i < numDisks-2; i++ ) {
			disks.elementAt(i).rf = 0;
			disks.elementAt(i).zf = 0;
			disks.elementAt(i).flowVelocity = 0;
			disks.elementAt(i).r2inv = 1.0/(disks.elementAt(i).r*disks.elementAt(i).r);
			r = disks.elementAt(i).r;
			rm1 = disks.elementAt(i-1).r;
			rm2 = disks.elementAt(i-2).r;
			rp1 = disks.elementAt(i+1).r;
			rp2 = disks.elementAt(i+2).r;
			//precompute derivatives along the tube, i.e. dM = dR/dz
			disks.elementAt(i).dM = (rp1-rm1)*invdistMx2;
			disks.elementAt(i).dM2 = (rp1-2*r+rm1)*invdistM2;
			disks.elementAt(i).dM3 = (rp2-2*rp1+2*rm1-rm2)*invdistM3x2;
			disks.elementAt(i).dM4 = (rp2-4*rp1+6*r-4*rm1+rm2)*invdistM4;
		}
		
		// Thread[] t = new Thread[numThreads];
		// int start = 2;
		// int end = numDisks-2;
		// for(int j = 0; j < numThreads; j++) {
		// 	int start_j = (end-start)*j/numThreads+start;
		// 	int end_j = (end-start)*(j+1)/numThreads-1+(j+1)/numThreads+start;
		// 	t[j] = new Thread(new InnerUpdate(start_j,end_j,this));
		// 	t[j].start();
		// }
		// try { for(int j = 0; j < numThreads; j++) { t[j].join(); } } catch (Exception e) { e.printStackTrace(); }
		//System.exit(0);
		//end cases for fixed r boundary conditions
		int i = 0;
		disks.elementAt(i).rf = 0;
		disks.elementAt(i).zf = 0;
		disks.elementAt(i).flowVelocity = 0;
		disks.elementAt(i).r2inv = 1.0/(disks.elementAt(i).r*disks.elementAt(i).r);
		r = disks.elementAt(i).r;
		rm1 = disks.elementAt(i).r;
		rm2 = disks.elementAt(i).r;
		rp1 = disks.elementAt(i+1).r;
		rp2 = disks.elementAt(i+2).r;
		disks.elementAt(i).dM = (rp1-rm1)*invdistMx2;
		disks.elementAt(i).dM2 = (rp1-2*r+rm1)*invdistM2;
		disks.elementAt(i).dM3 = (rp2-2*rp1+2*rm1-rm2)*invdistM3x2;
		disks.elementAt(i).dM4 = (rp2-4*rp1+6*r-4*rm1+rm2)*invdistM4;
		i = 1;
		disks.elementAt(i).rf = 0;
		disks.elementAt(i).zf = 0;
		disks.elementAt(i).flowVelocity = 0;
		disks.elementAt(i).r2inv = 1.0/(disks.elementAt(i).r*disks.elementAt(i).r);
		r = disks.elementAt(i).r;
		rm1 = disks.elementAt(i-1).r;
		rm2 = disks.elementAt(i-1).r;
		rp1 = disks.elementAt(i+1).r;
		rp2 = disks.elementAt(i+2).r;
		disks.elementAt(i).dM = (rp1-rm1)*invdistMx2;
		disks.elementAt(i).dM2 = (rp1-2*r+rm1)*invdistM2;
		disks.elementAt(i).dM3 = (rp2-2*rp1+2*rm1-rm2)*invdistM3x2;
		disks.elementAt(i).dM4 = (rp2-4*rp1+6*r-4*rm1+rm2)*invdistM4;
		i = numDisks-2;
		disks.elementAt(i).rf = 0;
		disks.elementAt(i).zf = 0;
		disks.elementAt(i).flowVelocity = 0;
		disks.elementAt(i).r2inv = 1.0/(disks.elementAt(i).r*disks.elementAt(i).r);
		r = disks.elementAt(i).r;
		rm1 = disks.elementAt(i-1).r;
		rm2 = disks.elementAt(i-2).r;
		rp1 = disks.elementAt(i+1).r;
		rp2 = disks.elementAt(i+1).r;
		disks.elementAt(i).dM = (rp1-rm1)*invdistMx2;
		disks.elementAt(i).dM2 = (rp1-2*r+rm1)*invdistM2;
		disks.elementAt(i).dM3 = (rp2-2*rp1+2*rm1-rm2)*invdistM3x2;
		disks.elementAt(i).dM4 = (rp2-4*rp1+6*r-4*rm1+rm2)*invdistM4;
		i = numDisks-1;
		disks.elementAt(i).rf = 0;
		disks.elementAt(i).zf = 0;
		disks.elementAt(i).flowVelocity = 0;
		disks.elementAt(i).r2inv = 1.0/(disks.elementAt(i).r*disks.elementAt(i).r);
		r = disks.elementAt(i).r;
		rm1 = disks.elementAt(i-1).r;
		rm2 = disks.elementAt(i-2).r;
		rp1 = disks.elementAt(i).r;
		rp2 = disks.elementAt(i).r;
		disks.elementAt(i).dM = (rp1-rm1)*invdistMx2;
		disks.elementAt(i).dM2 = (rp1-2*r+rm1)*invdistM2;
		disks.elementAt(i).dM3 = (rp2-2*rp1+2*rm1-rm2)*invdistM3x2;
		disks.elementAt(i).dM4 = (rp2-4*rp1+6*r-4*rm1+rm2)*invdistM4;	
		//conditions where there are no disks
		// i = -1;
		// r = disks.elementAt(i+1).r;
		// rm1 = disks.elementAt(i+1).r;
		// rm2 = disks.elementAt(i+1).r;
		// rp1 = disks.elementAt(i+1).r;
		// rp2 = disks.elementAt(i+2).r;
		// dMm = 0;
		// dM2m = 0;
		// dM3m = (rp2-rm2)*invdistM3x2;
		// dM4m = (rp2-rm2)*invdistM4;
		// i = numDisks;
		// r = disks.elementAt(i-1).r;
		// rm1 = disks.elementAt(i-1).r;
		// rm2 = disks.elementAt(i-2).r;
		// rp1 = disks.elementAt(i-1).r;
		// rp2 = disks.elementAt(i-1).r;
		// dMp = 0;
		// dM2p = 0;
		// dM3p = (-rp2+rm2)*invdistM3x2;
		// dM4p = (-rp2+rm2)*invdistM4;	
	}
	
	//returns the index of the disk containing the value z within it
	public int getDiskIndex(double z) {
		return (int)((z-beginZ)/(endZ-beginZ)*numDisks);
	}
	
	public double getSmoothedRadius(int memIndex, Bead bead) {
		double dr = disks.elementAt(memIndex+1).r-disks.elementAt(memIndex).r;
		double dz = bead.z-disks.elementAt(memIndex).z;
		return dr*dz/distM+disks.elementAt(memIndex).r;
	}
	
	public void applyHelfrich() {
		double energy = 0;
		for(int i = 0; i < disks.size(); i++) {
			Disk disk = disks.elementAt(i);
			disk.rf += Math.PI * Param.membrane_stiffness * disk.r2inv;
			disk.rf += -2 * Math.PI * Param.membrane_tension;
			disk.rf += -Math.PI * Param.membrane_stiffness * 2 * ( 1.5 * disk.dM2*disk.dM2 + 2 * disk.dM*disk.dM3 + disk.r*disk.dM4 );
			//gaussian term drops out because of cylidrical symmetry
						
			//note this is the full energy (not per unit length)
			energy += Math.PI * disk.r * (2*Param.membrane_tension + Param.membrane_stiffness * (disk.r2inv + disk.dM2*disk.dM2));
		}
		Logger.memE = energy;
		//Disk disk = disks.elementAt(30);
		//System.out.println(Math.PI+" "+Param.membrane_tension+" "+Param.membrane_stiffness);
		//System.out.println(energy+" "+(disk.dM2*disk.dM2)+" "+disk.dM*disk.dM3+" "+disk.r*disk.dM4);
		//System.exit(0);
	}
	
	
}
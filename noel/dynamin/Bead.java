package noel.dynamin;
import noel.util.math.*;
import java.util.*;

class Bead {
	
	MotorModule[] modules;
	Bead next = null;
	int filamentIndex; //which filament am I in?
	int index; //which bead in the filament am I?
	String str; //first column in input file
	public Coordinates coord;
	public double r,p,z;
	public double r2,rinv,rinv2;
	public double rf,pf,zf;
	public double fx,fy,fz;
	public double h,dp,dpinv,dhdz,dzdp,dhdp,tau,kappa,tauDiff,kappaDiff,A,B,distance,diffx,diffy,diffz;
	public double sigma,dtaudxi,dkappadxi,dAdxi,dBdxi,dAdzdxi,dAdpdxi,dBdzdxi,dBdpdxi; //coords for Geodesic
	public double velr,velp,velx,vely,velz; //velocities
	public double sin,cos;
	
	public int underlyingMembraneDisk = -1; 	//associated membrane disk
		
	public Bead(Coordinates coord, int filamentIndex, int index) {
		modules = new MotorModule[2];
		modules[Param.UP] = new MotorModule(Param.UP,this);
		modules[Param.DOWN] = new MotorModule(Param.DOWN,this);
		this.coord = coord;
		this.filamentIndex = filamentIndex;
		this.index = index;
	}
	
	//local variables
	public void update() {
		r = Math.sqrt( coord.x*coord.x + coord.y * coord.y);
		p = Math.atan2( coord.y, coord.x);
		z = coord.z;
		r2 = r*r;
		rinv = 1.0/r;
		rf = 0;
		pf = 0;
		zf = 0;
		fx = 0;
		fy = 0;
		fz = 0;
		modules[0].update();
		modules[1].update();
	}
	//dependent variables
	public void update2() {
		h = next.z - z;
		dp = next.p - p;
		if(dp>Math.PI) {dp = dp - 2*Math.PI;}
		if(dp<-Math.PI) {dp = dp + 2*Math.PI;}
		dpinv = 1.0/dp;
		dhdz = dpinv;
		dzdp = h*dpinv; //more accurate version of h
		dhdp = dzdp*dpinv;
		double denominv = 1.0/(dzdp*dzdp + r2);
		tau = dzdp * denominv;
		kappa = r * denominv;
		tauDiff =  tau - Param.spontaneous_twist;
		kappaDiff =  kappa - Param.spontaneous_curvature;
		double denom2inv = 1.0/((r2+dzdp*dzdp)*(r2+dzdp*dzdp));
		A = (r2-dzdp*dzdp)*denom2inv; //dtau/dx && -dkappa/dx
		B = -2*dzdp*r*denom2inv;		  
	}
	
	public String toString() {
		return "I am a bead. My index is "+index+" on filament "+filamentIndex;
	}
	
	public void setNeighbor(Bead bead) {
		next = bead;
	}
	
	public void updateMembraneDisk() {
		underlyingMembraneDisk = Simulation.membrane.getDiskIndex(z);
		if(underlyingMembraneDisk == 0 || underlyingMembraneDisk == (Simulation.membrane.disks.size()-1)) {
			Simulation.quit(Simulation.END_OF_FILAMENT); //bead has hit the boundary, no good
		}
		
	}
}


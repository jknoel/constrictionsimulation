package noel.dynamin;
import java.util.*;
import noel.io.*;

class Simulation {
	
	public static Membrane membrane;
	public static Vector<Filament> filamentList;
	public static NeighborList neighbors; 
	static Random random;
	static MyJmolViewer viewer;
	static long step;
	static Input input;
	static double nextOutputTime = 0;
		
	public Simulation(Membrane mem, Vector<Filament> filaments) {
		this.membrane = mem;
		this.filamentList = filaments;
	}
	
	public static void main(String[] args) {

		//***** Read parameters from command line and config file
		Param.initializeParameters(args);
		//********************************************

		//***** Initial coordinates ******************
		if(!Param.useCodeForInput) {
			System.out.println("Reading coordinates from file: "+Param.inputfile);
			input = new Input(Param.inputfile,Param.XYZ);
		} else { //can also initialize with the Input() constructor by setting 'useCodeForInput true'
			input = new Input();
		}
		membrane = input.mem;
		filamentList = input.filaments;
		//********************************************
		
		//***** Output coordinates *******************
		System.out.println("Printing coordinates to file: "+Param.outputfile);
		FileIO out = new FileIO(Param.outputfile,FileIO.WRITING);
		//********************************************

		//***** Set up some necessities **************
		// neighbor list
		NeighborList list = new NeighborList(membrane,filamentList,Param.repulsionCutoff);
		// applying forces to move particles 
		Integrator.membrane = membrane;
		Integrator.filamentList = filamentList;
		Integrator.timestep = Param.timestep;
		// Logger prints things to files and to the screen as requested
		Logger.initialize();
		Logger.filamentList = filamentList;
		//********************************************
		
		//**** Jmol viewer ***************************
		try { if(Param.viewerOn) viewer = new MyJmolViewer(); }
		catch (NoClassDefFoundError e) { Simulation.quit(Simulation.JMOL); }
		//********************************************

		//***** Main simulation loop **************
		for(step = 0; step < Param.steps; step++) {
			Param.currentStep = step;
			//***** Compute functions of the coordinates that are useful later *****
			Logger.begin();
			updateCoordinates();
			Logger.end(Logger.UPDATE_COORDINATES);
			//********************************************
			
			//***** Neighbor search every once in a while *****
			if( step % Param.neighborSearchStep == 0) {
				Logger.begin();
				list.updateNeighborList();
				Logger.end(Logger.NEIGHBOR_SEARCH);
			}
			//********************************************
			
			//***** Determine the forces *****************
			
			Logger.begin();
			for(int i = 0; i < filamentList.size(); i++) { 
				filamentList.elementAt(i).applyCurvature(); //filament curvature
				filamentList.elementAt(i).applyBonds();	//filament bonds (between nearest neighbor beads)
				filamentList.elementAt(i).applyTwist(); //filament twist
				filamentList.elementAt(i).applyGeo(); //filament geodesic curvature
			}
			Logger.end(Logger.FILAMENT_ELASTIC);
			
			Logger.begin();
			membrane.applyHelfrich(); //membrane elasticity
			applyInteraction(); //interaction between filament and membrane, i.e. attachment of filament to the membrane
			Logger.end(Logger.MEMBRANE_ELASTIC);
			
			Logger.begin();
			Filament.applyBeadRepulsion(list);
			Logger.end(Logger.REPULSION);
			
			Logger.begin();
			Filament.applyDimerInteraction(list);
			Logger.end(Logger.MM_DIMER_INTERACTION);
			
			//update positions (includes random forces for filament beads)
			Logger.begin();
			Integrator.integrate();
			Logger.end(Logger.INTEGRATE);
			
			Logger.begin();
			Filament.makeStateChanges(filamentList,list);
			Logger.end(Logger.MARKOV_STATES);
			
			//if( step % Param.outputStep==0) {
			if( Integrator.timeElapsed > nextOutputTime) {
				nextOutputTime += Param.outputStep;
				//Logger.printStates(filamentList);
				//Logger.printTimeKeeper(Param.startTime);				
				Logger.printE();
				Logger.printMinR(membrane);
				input.output(out,filamentList,membrane);
				if(Param.viewerOn) {
					FileIO tempOut = new FileIO("currentFrame.xyz",FileIO.WRITING);
					input.output(tempOut,filamentList,membrane);
					viewer.setFile("currentFrame.xyz",list);
				}
				
			}
		}
		System.out.println("******** END OF SIMULATION *************");
		Logger.printTimeKeeper(Param.startTimeNanos);
		System.out.println("Dumping last frame into file: lastFrame.xyz");
		FileIO lastFrame = new FileIO("lastFrame.xyz",FileIO.WRITING);
		input.output(lastFrame,filamentList,membrane);
	}
	
	//Compute functions of the coordinates that are useful for force calculations
	static void updateCoordinates() {
		membrane.update();
		for(int i = 0; i < filamentList.size(); i++) { filamentList.elementAt(i).update(); }
	}
	
	/**
	* Approximation to exponential probability distribution valid for rate*dt << 1. Returns true when an exponential process
	* occurs within time interval dt. CDF(t) = 1 - exp(-rate * t) ~ rate * dt for small dt. Time step here is unlikely to get
	* larger than 0.05 due to membrane integration and largest kinetic rate is binding/unbinding of GTP dimers at 0.3. Thus, 
	* indeed, rate*dt << 1.
	*/
	static boolean isMarkovMoveTaken(double rate, double dt) {
		if(rate == 0) { return false; }
		double r = random.nextDouble();
		if(r < dt * rate) return true; else return false;
	}
	
	/**
	* Interaction force between membrane and dynamin filament. Must be applied last as the total z forces on the beads
	* are applied also onto the membrane disks.
	*/
	static void applyInteraction() {
		double energy = 0;
		for(int i = 0; i < filamentList.size(); i++) { 
			Vector<Bead> beads = filamentList.elementAt(i).beads;
			for(int j = 0; j < beads.size(); j++) { 
				Bead bead = beads.elementAt(j);
				int membraneDisk = bead.underlyingMembraneDisk;
				double smoothedMembraneRadius = Simulation.membrane.getSmoothedRadius(membraneDisk,bead);
				double radius = bead.r;
				double diff = (radius - smoothedMembraneRadius - Param.membraneFilamentSeparation);
				double force = Param.membraneFilament_stiffness * diff;
				energy += Param.membraneFilament_stiffness * diff * diff;
				bead.rf += -force;
				membrane.disks.elementAt(membraneDisk).rf += force * Param.stalkDimerSeparation/Param.membraneDiskThickness;
				double force_z = force * Simulation.membrane.disks.elementAt(membraneDisk).dM;
				bead.zf += force_z;
				membrane.disks.elementAt(membraneDisk).zf += beads.elementAt(j).zf; //bead is embedded in membrane and drags along it
			}
		}
		Logger.intE = energy;
	}
	
	
	//************** Error handling **************
	final static int JMOL = 0;
	final static int END_OF_FILAMENT = 1;
	final static int NEG_STEPS = 2;
	final static int NEG_OUTPUTSTEP = 3;
	final static int NO_INPUTFILE = 4;

	public static void quit(String s) {
		System.out.println(s);
		System.exit(0);
	}
	public static void quit(int s) {
		String str = "";
		System.out.println("\n******** Error **********");
		boolean printLast = false;
		switch (s) {
			case 0: str=("Jmol.jar is not found. It is used to display graphics during the simulation. You can:"+
				"\n1. change 'viewerOn true' to 'viewerOn false' in the configuration file."+
				"\n2. Make sure Jmol.jar is in your classpath, e.g.\n\t java -classpath \"$distroot:$distroot/Jmol.jar\""+ 
				" noel.dynamin.Simulation --config config viewerOn true"); printLast = true; break;
			case 1: str=("A simulation bead has reached one of the filament ends, i.e. hit the boundary of"+
				" the simulation\n."); printLast = true; break;
			case 2: str=("Parameter 'steps' is either negative or undefined. This parameter sets the total number of steps"+
				" for the simulation. For example, to run 1000 steps, add 'steps 1000'"+
				" to the command line arguments or the config file."); break;
			case 3: str=("Parameter 'outputStep' is either negative or undefined. This parameter sets the number of steps"+
				" between screen/file output. For example, to output every 100 steps, add 'outputStep 100' "+
				" to the command line arguments or the config file."); break;
			case 4: str=("No input file was given. Add 'inputfile name.xyz' to config file to provide a filename."+
				" Alternatively, one can edit the Input.java constructor to create an initial condition. For this"+
					" option add 'useCodeForInput true' to the config file and edit Input.java. Note,"+
						" this requires a recompile."); break;
		}
		System.out.println(str);
		if(printLast) { 
			FileIO lastFrame = new FileIO("lastFrame.xyz",FileIO.WRITING);
			input.output(lastFrame,filamentList,membrane);
			System.out.println("\nLast trajectory frame written to lastFrame.xyz");
		}
		System.out.println("\nQuitting.");
		System.exit(0);
	}

	
	
	
	
	
	
	
	
}
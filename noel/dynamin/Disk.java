package noel.dynamin;

	/**
	* Disk goes from [z to z+distM)
	*/
	class Disk {
		public double r;
		public double z;
		public double dM,dM2,dM3,dM4,r2inv;
		public double flowVelocity, effectiveForceTimesMobility;
		public double rf,zf; //actually stress, force/length_z
		
		public Disk(double radius, double z) {
			this.r = radius;
			this.z = z;
		}
	}
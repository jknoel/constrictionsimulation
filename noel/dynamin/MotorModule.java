package noel.dynamin;

import noel.util.math.*;

class MotorModule {
	int nucleotideState;
	int type;
	boolean isDimerized;
	int orientation; //up or down
	Bead myBead;
	MotorModule myPartner; //the G dimer partner when there is one
	int virtualMultiplier;
	double r,p,x,y,z; //store the coordinates so they can be accessed without recalculating
	
	//Virtual separation constants; motor module attachment point is displaced from center of bead
	private static final double DELTAR = 3.7; //nm
	private static final double RDELTAP = 3.6; //nm
	private static final double DELTAZ = 1; //nm
	
	public MotorModule(int orientation, Bead myBead) {
		if(Param.initialStateGTP) nucleotideState = Param.GTP;
		else nucleotideState = Param.APO;
		type = Param.WT;
		isDimerized = false;
		this.orientation = orientation;
		this.myBead = myBead;
		//virtualMultiplier is for getCoords; z and p depend on whether the MM is pointing up or down
		if(orientation == Param.UP) virtualMultiplier = 1;
		else virtualMultiplier = -1;
	}
	
	public String toString() {
		return "I am a motor module. I am "+getOrientationString()+" on bead "+myBead.index+" and filament "+myBead.filamentIndex;
	}
	public String getOrientationString() {
		switch (orientation) {
			case Param.UP: return "UP";
			case Param.DOWN: return "DOWN";
			default: return "UNKNOWN ORIENTATION";
		}
	}
	public void setNucleotideState(int newState) { nucleotideState = newState; }
	public void setDimer() { isDimerized = true; }
	public void setMonomer() { isDimerized = false; }
	
	public Coordinates getCoords() {
		x = r*Math.cos(p);
		y = r*Math.sin(p);
		return new Coordinates(x,y,z);
	}
	
	//returns from lower bead's (UP pointing) perspective
	public double getDeltaPhi(MotorModule mm) {
		//check that the UP MM has a smaller z
		double dp = 0;
		try {
		if(orientation == mm.orientation) { 
			Simulation.quit("called getDeltaPhi for two MM with same orientation"); 
		}
		if(orientation == Param.UP) {
			dp = p - mm.p;
		} else if(orientation == Param.DOWN) {
			dp = mm.p - p;
		} else {
			System.out.println(myBead.coord+" "+z);
			System.out.println(mm.myBead.coord+" "+mm.z);			
			System.out.println(orientation+" "+z+" "+myBead.filamentIndex+" "+myBead.index+" "+mm.orientation+" "+mm.z+" "+mm.myBead.filamentIndex+" "+mm.myBead.index);
			throw new Exception();
		}
		} catch (Exception e) { 
			e.printStackTrace(); 			
			Simulation.quit("called getDeltaPhi with wrong z positioning wrt UP/DOWN");
		}
		if(dp > Math.PI) {dp = dp - 2*Math.PI;}
		if(dp < - Math.PI) {dp = dp + 2*Math.PI;}
		return dp;
	}
	
	//update coordinate stuff each timestep
	public void update() {
		r = myBead.r + DELTAR;
		p = myBead.p - virtualMultiplier*RDELTAP/r;
		z = myBead.z + virtualMultiplier*DELTAZ;
	}
	
	public boolean isGTPDimerValid() {
		if(! isDimerized) return false;
		double distance = Coordinates.distance(getCoords(),myPartner.getCoords());
		if(distance > Param.mmDimer_maxLength) return false;
		else return true;
	}
	
	//if dimer is pulled too far, it should dissociate
	public boolean isGDPPIDimerValid() {
		if(! isDimerized) return false;
		double distance = Coordinates.distance(getCoords(),myPartner.getCoords());
		if(distance > Param.mmDimer_breakingLength) return false;
		else return true;
	}
	
	//depending on deltaPhi, perhaps the dimer dissociates more quickly; i.e. strain dependence
	public double getAllostericFactor() {
		if(! isDimerized) return 1;
		if(getDeltaPhi(this.myPartner) < 0) { 
			//System.out.println("fast dissociation "+toString());
			return 100; //dissociate much more quickly
		}
		else return 1;
	}
	
}
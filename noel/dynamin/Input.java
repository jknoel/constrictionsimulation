package noel.dynamin;
import noel.io.*;
import java.util.*;
import noel.util.math.*;
/**
* Input with special XYZ format
* 
*/

class Input {
	
	FileIO input;
	Membrane mem;
	Vector<Filament> filaments;
		
	//input from file
	public Input(String inputFile,int format) {
		String line = "Error";
		if(format==Param.XYZ) {
			input = new FileIO(inputFile,FileIO.BUFFERED_READING);
			input.readLine(); //skip line (total number of beads)			
			String[] tokens = FileIO.getTokens(input.readLine()," "); //chain lengths
			int[] lengths = new int[tokens.length];
			int numFilament = tokens.length-1;
			filaments = new Vector<Filament>();
			for(int i = 0; i < numFilament; i++) { //loop over filaments
				lengths[i] = Integer.parseInt(tokens[i]);
				filaments.add(new Filament(i,Param.curvature_modulus,Param.twist_modulus,Param.stretching_modulus,Param.stalkDimerSeparation,Param.gauss_modulus));
			}
			//System.out.println(lengths[0]);
			lengths[tokens.length-1] = Integer.parseInt(tokens[tokens.length-1]); //num membrane is last
			//read in filaments
			for(int j = 0; j < numFilament; j++) {
				for(int i = 0; i < lengths[j]; i++) {
					tokens = FileIO.getTokens(input.readLine()," ");
					double x = Double.parseDouble(tokens[1]);
					double y = Double.parseDouble(tokens[2]);
					double z = Double.parseDouble(tokens[3]);										
					Coordinates coord = new Coordinates(x,y,z);
					Bead bead = new Bead(coord,j,i);
					bead.str = tokens[0];
					filaments.elementAt(j).add(bead);
					//System.out.println(coord);
				}
			}
			//read in membrane
			int membraneLength = lengths[lengths.length-1];
			double[] radii = new double[membraneLength];
			double[] z = new double[membraneLength];
			for(int i = 0; i < membraneLength; i++) {
				tokens = FileIO.getTokens(input.readLine()," ");
				radii[i] = Double.parseDouble(tokens[1]);
				z[i] = Double.parseDouble(tokens[3]);										
				//System.out.println(radii[i]+" "+z[i]);
			}
			mem = new Membrane(radii,z);
			for(int i = 0; i < filaments.size(); i++) { filaments.elementAt(i).setMembraneTube(mem); }
		}
	}
	
	//create input from create() method
	public Input() {
		create();
	}
	
	/**
	* Output with special XYZ format
	*/
	public static void output(FileIO out, Vector<Filament> filamentList, Membrane membrane) {
		int total = 0;
		String individualCounts = "";
		for(int i = 0; i < filamentList.size(); i++) { 
			individualCounts+=filamentList.elementAt(i).beads.size()+" ";
			total+=filamentList.elementAt(i).beads.size();
		}
		individualCounts+=membrane.numDisks+"\n";
		total+=membrane.numDisks;
		out.write(total+"\n");
		out.write(individualCounts);
		for(int i = 0; i < filamentList.size(); i++) {
			Vector<Bead> beads = filamentList.elementAt(i).beads;
			for(int j = 0; j < beads.size(); j++) {
				Bead bead = beads.elementAt(j);
				out.write(bead.str+" "+bead.coord.toString()+"\n");
			}
		}
		for(int i = 0; i < membrane.disks.size(); i++) {
			out.write("O "+membrane.disks.elementAt(i).r+" 0 "+membrane.disks.elementAt(i).z+"\n");
		}
	}
	
	/**
	* Generates an initial condition with a straight tube
	*/
	public void create() {
		//This is a skeleton, change as needed
		
		//add a membrane
		int membraneLength = 150; //i.e. numDisks
		double memStart = -membraneLength*Param.membraneDiskThickness/2;
		double memR = 12; //Math.sqrt(Param.membrane_stiffness/2/Param.membrane_tension);
		double[] radii = new double[membraneLength];
		double[] zs = new double[membraneLength];
		for(int i = 0; i < membraneLength; i++) {
			radii[i] = memR;
			zs[i] = memStart+i*Param.membraneDiskThickness;									
		}
		mem = new Membrane(radii,zs);
		
		filaments = new Vector<Filament>();

		//add a filament
		int filamentIndex = 0;
		filaments.add(new Filament(filamentIndex,Param.curvature_modulus,Param.twist_modulus,Param.stretching_modulus,Param.stalkDimerSeparation,Param.gauss_modulus));
		int numBeads = 900;
		double radius = memR + Param.membraneFilamentSeparation;
		double pitch = 10;
		double h = pitch / 2 / Math.PI;
		double dtheta = Param.stalkDimerSeparation / Math.sqrt(h*h+radius*radius);
		double xi = 0;
		double yi = 0;
		double zi = -200;
		for(int i = 0; i < numBeads; i++) {
			double theta = dtheta*i;
			double x = radius * Math.cos(theta) + xi;
			double y = radius * Math.sin(theta) + yi;
			double z = theta / 2 / Math.PI * pitch + zi;							
			Coordinates coord = new Coordinates(x,y,z);
			Bead bead = new Bead(coord,filamentIndex,i);
			bead.str = "CA";
			filaments.elementAt(filamentIndex).add(bead);
		}
		
		//add another filament
		// filamentIndex = 1;
		// filaments.add(new Filament(filamentIndex,Param.curvature_modulus,Param.twist_modulus,Param.stretching_modulus,Param.stalkDimerSeparation,Param.gauss_modulus));
		// numBeads = 10;
		// radius = memR + Param.membraneFilamentSeparation;
		// pitch = 20;
		// h = pitch / 2 / Math.PI;
		// dtheta = Param.stalkDimerSeparation / Math.sqrt(h*h+radius*radius);
		// xi = 0;
		// yi = 0;
		// zi = 5;
		// for(int i = 0; i < numBeads; i++) {
		// 	double theta = dtheta*i;
		// 	double x = radius * Math.cos(theta) + xi;
		// 	double y = radius * Math.sin(theta) + yi;
		// 	double z = theta / 2 / Math.PI * pitch + zi;
		// 	Coordinates coord = new Coordinates(x,y,z);
		// 	Bead bead = new Bead(coord,filamentIndex,i);
		// 	bead.str = "D2";
		// 	filaments.elementAt(filamentIndex).add(bead);
		//}
		
		for(int i = 0; i < filaments.size(); i++) { filaments.elementAt(i).setMembraneTube(mem); }
	}
	

	
	
}
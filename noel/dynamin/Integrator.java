package noel.dynamin;

import noel.io.*;
import java.util.*;
import noel.util.math.*;

class Integrator {
	
	static Membrane membrane;
	static Vector<Filament> filamentList;
	public static double timestep;
	static boolean decreaseTimestep;
	static double timeElapsed = 0;
	
	static void integrate() {
		decreaseTimestep = false;
		integrateFilament();
		integrateMembrane();
		timeElapsed += timestep;
		if(Param.variableTimestepOn) {
			if(decreaseTimestep) timestep = timestep * 0.9;
			else timestep = timestep * 1.001;
		}
	}
	
	/**
	* Moves the Beads forward one step of size timestep. Random forces are included.
	*/
	static void integrateFilament() {
		for(int i = 0; i < filamentList.size(); i++) { 
			Vector<Bead> beads = filamentList.elementAt(i).beads;
			for(int j = 0; j < beads.size(); j++) {
				//first sum forces into cartesian coordinates
				Bead bead = beads.elementAt(j);
				bead.sin = Math.sin(bead.p);
				bead.cos = Math.cos(bead.p);
				bead.fx += bead.rf*bead.cos - bead.pf*bead.sin;
				bead.fy += bead.rf*bead.sin + bead.pf*bead.cos;
				bead.fz += bead.zf;
				//convert back to cylindrical
				bead.rf = bead.fx*bead.cos + bead.fy*bead.sin;
				bead.pf = bead.fy*bead.cos - bead.fx*bead.sin;
				bead.zf = bead.fz;
				if(Param.output_torque) { Logger.logTotalTorque(bead); }
				//calculate velocities
				bead.velr = bead.rf * Param.mobility[0];
				bead.velp = bead.pf * Param.mobility[1];
				bead.velz = bead.zf * Param.mobility[2];
				//add random forces
				if(Param.tempOn) {
					bead.velr += Param.rf_coeff[0] * Simulation.random.nextGaussian();
					bead.velp += Param.rf_coeff[1] * Simulation.random.nextGaussian();
					bead.velz += Param.rf_coeff[2] * Simulation.random.nextGaussian();
				}
				//if considering flows, the embedded beads experience the membrane flow
				if(Param.viscosityOn) bead.velz += membrane.disks.elementAt(bead.underlyingMembraneDisk).flowVelocity;	
				// convert to cartesian velocities
				bead.velx = bead.velr*bead.cos - bead.velp*bead.sin;					
				bead.vely = bead.velr*bead.sin + bead.velp*bead.cos;
				//move forward
				double dx = bead.velx * timestep;
				double dy = bead.vely * timestep;
				double dz = bead.velz * timestep;
				bead.coord.x = bead.coord.x + dx;
				bead.coord.y = bead.coord.y + dy;
				bead.coord.z = bead.coord.z + dz;
				if(Param.variableTimestepOn) {
					//record maximum velocity
					double displacementSq = dx*dx+dy*dy+dz*dz;
					if(displacementSq > Param.maximumSqDisplacement) { decreaseTimestep = true; }
				}
			}
		}
	}
	
	/**
	* Moves the Disks forward one step of size timestep. Random forces are not included.
	*/
	static void integrateMembrane() {
		double distM = Param.membraneDiskThickness;
		Vector<Disk> disks = membrane.disks;
		if(Param.viscosityOn) { //continuity equation integration
			//compute z velocities with continuity
			for(int i = 1; i < disks.size()-1; i++) {
				Disk disk = disks.elementAt(i);
				Disk disk_m = disks.elementAt(i-1);
				Disk disk_p = disks.elementAt(i+1);
				//flow generated from differential stress
				disk.flowVelocity += Param.membrane2DMobility * ( disk_p.r*disk_p.rf - disk_m.r*disk_m.rf ) / (2*distM);
				//flow generated from beads dragging
				//IMPORTANT this will be shared with bead updateCoordinates, so call membrane first!!
				disk.flowVelocity += Param.membrane2DMobility * disk.zf / distM; 
			}
			//boundary terms
			disks.elementAt(0).flowVelocity = disks.elementAt(1).flowVelocity;
			disks.elementAt(disks.size()-1).flowVelocity = disks.elementAt(disks.size()-2).flowVelocity;			
			//z velocity done, now compute effective radial force from continuity equation
	  	  	for(int i = 1; i < disks.size()-1; i++) {
				Disk disk = disks.elementAt(i);
				Disk disk_m = disks.elementAt(i-1);
				Disk disk_p = disks.elementAt(i+1);
				disk.effectiveForceTimesMobility = - ( disk_p.r*disk_p.flowVelocity - disk_m.r*disk_m.flowVelocity ) / (2*distM); //i.e. radial velocity
			}
			//boundary terms: set to zero since they are fixed
			disks.elementAt(0).effectiveForceTimesMobility = 0;
			disks.elementAt(disks.size()-1).effectiveForceTimesMobility = 0;
		} else { //no viscosityOn
	  	  	for(int i = 0; i < disks.size(); i++) {
				Disk disk = disks.elementAt(i);
				disk.effectiveForceTimesMobility = disk.rf * Param.membrane2DMobility; //i.e. radial velocity			
			}
		}
		//move forward
		for(int i = 0; i < disks.size(); i++) {
			Disk disk = disks.elementAt(i);
			double dr = disk.effectiveForceTimesMobility * timestep;
			disk.r = disk.r + dr;
			if(Param.variableTimestepOn) {
				if(dr*dr > Param.maximumSqDisplacement) { decreaseTimestep = true; }
			}
		}		
	}
}
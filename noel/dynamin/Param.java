package noel.dynamin;
import java.util.*;
import noel.util.math.*;
import argparser.*;
import java.io.*;

class Param {
	
	/**********************
	* Constants
	***********************/
	// nucleotideState
	final static int APO = 0;
	final static int GTP = 1;
	final static int GTPD = 2;
	final static int GDPPID = 3;
	final static int GDP = 4;
	//MM type
	final static int WT = 0;
	final static int UP = 0; //MM pointing up
	final static int DOWN = 1; //MM pointing down
	//file format
	final static int XYZ = 1;
	
	static boolean forceLogging = false;
	static int forceLoggingRate = 10;
	static String forceLoggingFile = "forces";
	
	static long startTime;
	static long startTimeNanos;
	
	static long currentStep = -1;
	
	//Simulation parameters
	static double spontaneous_twist, spontaneous_curvature, membraneFilamentSeparation, curvature_modulus,
			 twist_modulus, gauss_modulus, stalkDimerSeparation, stretching_modulus, membrane_stiffness, 
			 membrane_tension, mmDimer_maxLength, mmDimer_constantForce, mmDimer_zeroForceLength, 
			 repulsionStiffness, repulsionCutoff, membraneFilament_stiffness, mmDimer_breakingLength, 
			 timestep, boltzmann_constant, temperature, kT, kT_unit, membrane2DMobility, 
			 rate_APO_GTP, rate_APO_GDP, rate_GTP_APO, 
			 rate_GDP_APO, rate_GTP_GTPD, rate_GTPD_GTP, rate_GTPD_GDPPID, rate_GDPPID_GDP, 
			 neighborSearchDistance, membraneDiskThickness, GTP_concentration, GDP_concentration, 
			 maximumSqDisplacement;
	static int neighborSearchStep, outputStep, output_torque_step;
	static long steps, random_seed;
	static boolean viscosityOn, viewerOn, variableTimestepOn, useCodeForInput, tempOn;
	static boolean output_torque, initialStateGTP;
	static String inputfile, outputfile, config;
	static double[] mobility = new double[3]; //r,p,z
	static double[] rf_coeff = new double[3]; //r,p,z

	//Argparser things
	static DoubleHolder spontaneous_twist_holder, spontaneous_curvature_holder, membraneFilamentSeparation_holder, curvature_modulus_holder,
		 twist_modulus_holder, gauss_modulus_holder, stalkDimerSeparation_holder, stretching_modulus_holder, membrane_stiffness_holder, 
		 membrane_tension_holder, mmDimer_maxLength_holder, mmDimer_constantForce_holder, mmDimer_zeroForceLength_holder, 
		 repulsionStiffness_holder, repulsionCutoff_holder, membraneFilament_stiffness_holder, mmDimer_breakingLength_holder, 
		 timestep_holder, boltzmann_constant_holder, temperature_holder, membrane2DMobility_holder, 
		 mobility_holder, rate_APO_GTP_holder, rate_APO_GDP_holder, rate_GTP_APO_holder, rate_GDP_APO_holder, rate_GTP_GTPD_holder, 
		 rate_GTPD_GTP_holder, rate_GTPD_GDPPID_holder, rate_GDPPID_GDP_holder, neighborSearchDistance_holder, 
		 membraneDiskThickness_holder, GTP_concentration_holder, GDP_concentration_holder, maximumSqDisplacement_holder;
	static IntHolder neighborSearchStep_holder, outputStep_holder, output_torque_step_holder;
	static LongHolder steps_holder, random_seed_holder;
	static BooleanHolder viscosityOn_holder, viewerOn_holder, variableTimestepOn_holder, useCodeForInput_holder, output_torque_holder, tempOn_holder, initialStateGTP_holder;
	static StringHolder inputfile_holder, outputfile_holder, config_holder;

	
	public static void initializeParameters(String[] args) {
		startTime = System.currentTimeMillis(); //this is now
		startTimeNanos = System.nanoTime();
		
		//Argparser initialize parameter holders
		spontaneous_twist_holder = new DoubleHolder();
		spontaneous_curvature_holder = new DoubleHolder();
		membraneFilamentSeparation_holder = new DoubleHolder();
		curvature_modulus_holder = new DoubleHolder();
		twist_modulus_holder = new DoubleHolder();
		gauss_modulus_holder = new DoubleHolder();
		stalkDimerSeparation_holder = new DoubleHolder();
		stretching_modulus_holder = new DoubleHolder();
		membrane_stiffness_holder = new DoubleHolder();
		membrane_tension_holder = new DoubleHolder();
		mmDimer_maxLength_holder = new DoubleHolder();
		mmDimer_constantForce_holder = new DoubleHolder();
		mmDimer_zeroForceLength_holder = new DoubleHolder();
		repulsionStiffness_holder = new DoubleHolder();
		repulsionCutoff_holder = new DoubleHolder();
		membraneFilament_stiffness_holder = new DoubleHolder();
		mmDimer_breakingLength_holder = new DoubleHolder();
		timestep_holder = new DoubleHolder();
		random_seed_holder = new LongHolder();
		boltzmann_constant_holder = new DoubleHolder();
		temperature_holder = new DoubleHolder();
		tempOn_holder = new BooleanHolder();
		membrane2DMobility_holder = new DoubleHolder();
		mobility_holder = new DoubleHolder();
		rate_APO_GTP_holder = new DoubleHolder();
		rate_APO_GDP_holder = new DoubleHolder();
		rate_GTP_APO_holder = new DoubleHolder();
		rate_GDP_APO_holder = new DoubleHolder();
		rate_GTP_GTPD_holder = new DoubleHolder();
		rate_GTPD_GTP_holder = new DoubleHolder();
		rate_GTPD_GDPPID_holder = new DoubleHolder();
		rate_GDPPID_GDP_holder = new DoubleHolder();
		initialStateGTP_holder = new BooleanHolder();
		neighborSearchDistance_holder = new DoubleHolder();
		membraneDiskThickness_holder = new DoubleHolder();
		GTP_concentration_holder = new DoubleHolder();
		GDP_concentration_holder = new DoubleHolder();
		neighborSearchStep_holder = new IntHolder();
		outputStep_holder = new IntHolder();
		steps_holder = new LongHolder();
		viscosityOn_holder = new BooleanHolder();
		config_holder = new StringHolder();
		outputfile_holder = new StringHolder();
		inputfile_holder = new StringHolder();
		viewerOn_holder = new BooleanHolder();
		useCodeForInput_holder = new BooleanHolder();
		variableTimestepOn_holder = new BooleanHolder();
		output_torque_holder = new BooleanHolder();
		output_torque_step_holder = new IntHolder();
		maximumSqDisplacement_holder = new DoubleHolder();
		
		
		ArgParser parser;
		String usageString = "java -classpath ~/Dropbox/git/noeljava noel.dynamin.Simulation --config <filename> [[other options]]\n\nVersion 1.0";
		parser = new ArgParser(usageString);
		
		parser.addOption("config,--config %s # configuration file containing parameter values",config_holder);
		parser.addOption("inputfile %s # filename of initial condition in XYZ format",inputfile_holder);
		parser.addOption("outputfile %s # filename for output coordinates",outputfile_holder);
		parser.addOption("spontaneous_twist %f #",spontaneous_twist_holder);
		parser.addOption("spontaneous_curvature %f #",spontaneous_curvature_holder);
		parser.addOption("membraneFilamentSeparation %f #",membraneFilamentSeparation_holder);
		parser.addOption("curvature_modulus %f #",curvature_modulus_holder);
		parser.addOption("twist_modulus %f #",twist_modulus_holder);
		parser.addOption("gauss_modulus %f #",gauss_modulus_holder);
		parser.addOption("stalkDimerSeparation %f #",stalkDimerSeparation_holder);
		parser.addOption("stretching_modulus %f #",stretching_modulus_holder);
		parser.addOption("membrane_stiffness %f #",membrane_stiffness_holder);
		parser.addOption("membrane_tension %f #",membrane_tension_holder);
		parser.addOption("mmDimer_maxLength %f #",mmDimer_maxLength_holder);
		parser.addOption("mmDimer_constantForce %f #",mmDimer_constantForce_holder);
		parser.addOption("mmDimer_zeroForceLength %f #",mmDimer_zeroForceLength_holder);
		parser.addOption("repulsionStiffness %f #",repulsionStiffness_holder);
		parser.addOption("repulsionCutoff %f #",repulsionCutoff_holder);
		parser.addOption("membraneFilament_stiffness %f #",membraneFilament_stiffness_holder);
		parser.addOption("mmDimer_breakingLength %f #",mmDimer_breakingLength_holder);
		parser.addOption("timestep %f #",timestep_holder);
		parser.addOption("boltzmann_constant %f #",boltzmann_constant_holder);
		parser.addOption("temperature %f #size of random forces (1~310K)",temperature_holder);
		parser.addOption("tempOn %b #toggles the random forces on/off",tempOn_holder);
		parser.addOption("random_seed %d # User can provide a random seed, otherwise uses current time",random_seed_holder);
		parser.addOption("membrane2DMobility %f #",membrane2DMobility_holder);
		parser.addOption("mobility %f #",mobility_holder);
		parser.addOption("viscosityOn %b #",viscosityOn_holder);
		parser.addOption("rate_APO_GTP %f #",rate_APO_GTP_holder);
		parser.addOption("rate_APO_GDP %f #",rate_APO_GDP_holder);
		parser.addOption("rate_GTP_APO %f #",rate_GTP_APO_holder);
		parser.addOption("rate_GDP_APO %f #",rate_GDP_APO_holder);
		parser.addOption("rate_GTP_GTPD %f #",rate_GTP_GTPD_holder);
		parser.addOption("rate_GTPD_GTP %f #",rate_GTPD_GTP_holder);
		parser.addOption("rate_GTPD_GDPPID %f #",rate_GTPD_GDPPID_holder);
		parser.addOption("rate_GDPPID_GDP %f #",rate_GDPPID_GDP_holder);
		parser.addOption("initialStateGTP %b #if false initial state is APO",initialStateGTP_holder);
		parser.addOption("neighborSearchStep %i #",neighborSearchStep_holder);
		parser.addOption("neighborSearchDistance %f #",neighborSearchDistance_holder);
		parser.addOption("steps %d #",steps_holder);
		parser.addOption("outputStep %i #",outputStep_holder);
		parser.addOption("membraneDiskThickness %f #",membraneDiskThickness_holder);
		parser.addOption("GTP_concentration %f #",GTP_concentration_holder);
		parser.addOption("GDP_concentration %f #",GDP_concentration_holder);
		parser.addOption("viewerOn %b #toggles the graphical viewer (Jmol) on/off",viewerOn_holder);
		parser.addOption("useCodeForInput %b #use Input.java constructor to create initial condition",useCodeForInput_holder);
		parser.addOption("variableTimestepOn %b #toggles timestep optimization on/off",variableTimestepOn_holder);
		parser.addOption("outputTorqueOn %b #toggles avg torque over 100 ms intervals to be printed to file 'torque'",output_torque_holder);
		parser.addOption("output_torque_step %i #",output_torque_step_holder);
		parser.addOption("maximumSqDisplacement %f #if variableTimestepOn target for per-step displacement maximum",maximumSqDisplacement_holder);
	
		//Set up default values, if they are not present in the config file
		//the parameters will take these values
		//********** REQUIRED PARAMETERS ******************
		steps_holder.value = -1;
		outputStep_holder.value = -1;
		inputfile_holder.value = "NO_FILENAME_SET";
		//********** PARAMETERS WITH FLAG VALUES **********
		random_seed_holder.value = -1; 
		//********** PARAMETERS WITH DEFAULT VALUES *******
		outputfile_holder.value = "output.xyz";
		spontaneous_twist_holder.value = 0.004;
		spontaneous_curvature_holder.value = 0.058800;
		membraneFilamentSeparation_holder.value = 8.5;
		curvature_modulus_holder.value = 1200;
		twist_modulus_holder.value = 1100;
		gauss_modulus_holder.value = twist_modulus;
		stalkDimerSeparation_holder.value = 5.6;
		stretching_modulus_holder.value = 170;
		membrane_stiffness_holder.value = 24;
		membrane_tension_holder.value = 0.03;
		mmDimer_maxLength_holder.value = 14;
		mmDimer_breakingLength_holder.value = 16;
		mmDimer_constantForce_holder.value = 1.3;
		mmDimer_zeroForceLength_holder.value = 7.5;
		repulsionStiffness_holder.value = 1;
		repulsionCutoff_holder.value = 9.5;
		membraneFilament_stiffness_holder.value = 1;
		boltzmann_constant_holder.value = 0.00831451;
		temperature_holder.value = 1;
		tempOn_holder.value = true;
		membrane2DMobility_holder.value = 1;
		viscosityOn_holder.value = true;
		neighborSearchStep_holder.value = 50;
		neighborSearchDistance_holder.value = 16;
		membraneDiskThickness_holder.value = 4;
		GTP_concentration_holder.value = 300;
		GDP_concentration_holder.value = 30;
		timestep_holder.value = 0.01;
		variableTimestepOn_holder.value = true;
		output_torque_holder.value = false;
		output_torque_step_holder.value = 100;
		maximumSqDisplacement_holder.value = 0.005;
		viewerOn_holder.value = false;
		useCodeForInput_holder.value = false;
		rate_APO_GTP_holder.value = 0.0000066;
		rate_APO_GDP_holder.value = 0.0000072;
		rate_GTP_APO_holder.value = 0.000108;
		rate_GDP_APO_holder.value = 0.000120;
		rate_GTP_GTPD_holder.value = 0.3;
		rate_GTPD_GTP_holder.value = 0.1;
		rate_GTPD_GDPPID_holder.value = 0.000175;
		rate_GDPPID_GDP_holder.value = 0.000032;
		initialStateGTP_holder.value = true;

		//********* Experimentally uncharacterized rates ********
		//these choices give
		//k^{spec}_{hyd} = 116 1/s, k_diss = 0.1 1/s

		//*******************************************************

        parser.matchAllArgs(args); //inital match to grab configuration file name
        if(config_holder.value == null) {parser.printErrorAndExit("Configuration file is required!\nUsage: --config [file]\n");}
        
		//read in configuration
		try {
			System.out.println("Parsing configuration file: "+config_holder.value);
			File configFile = new File(config_holder.value);
			if(!configFile.exists()) { parser.printErrorAndExit("Some problem with reading configuration file; maybe it doesn't exist?"); }
			args = parser.prependArgs(new File(config_holder.value),args);
		} catch (IOException e) {
			parser.printErrorAndExit("Some problem with reading configuration file; maybe it doesn't exist?");
		}
		parser.matchAllArgs(args); //match all args, command line args overwrite the config file
		
		//transfer parameter values to simulation variables
		steps = steps_holder.value;
		outputStep = outputStep_holder.value;
		spontaneous_twist = spontaneous_twist_holder.value;
		spontaneous_curvature = spontaneous_curvature_holder.value;
		membraneFilamentSeparation = membraneFilamentSeparation_holder.value;
		curvature_modulus = curvature_modulus_holder.value;
		twist_modulus = twist_modulus_holder.value;
		gauss_modulus = gauss_modulus_holder.value;
		stalkDimerSeparation = stalkDimerSeparation_holder.value;
		stretching_modulus = stretching_modulus_holder.value;
		membrane_stiffness = membrane_stiffness_holder.value;
		membrane_tension = membrane_tension_holder.value;
		mmDimer_maxLength = mmDimer_maxLength_holder.value;
		mmDimer_constantForce = mmDimer_constantForce_holder.value;
		mmDimer_zeroForceLength = mmDimer_zeroForceLength_holder.value;
		repulsionStiffness = repulsionStiffness_holder.value;
		repulsionCutoff = repulsionCutoff_holder.value;
		membraneFilament_stiffness = membraneFilament_stiffness_holder.value;
		mmDimer_breakingLength = mmDimer_breakingLength_holder.value;
		timestep = timestep_holder.value;
		boltzmann_constant = boltzmann_constant_holder.value;
		temperature = temperature_holder.value;
		tempOn = tempOn_holder.value;
		random_seed = random_seed_holder.value;
		membrane2DMobility = membrane2DMobility_holder.value;
		viscosityOn = viscosityOn_holder.value;
		initialStateGTP = initialStateGTP_holder.value;
		rate_APO_GTP = rate_APO_GTP_holder.value;
		rate_APO_GDP = rate_APO_GDP_holder.value;
		rate_GTP_APO = rate_GTP_APO_holder.value;
		rate_GDP_APO = rate_GDP_APO_holder.value;
		rate_GTP_GTPD = rate_GTP_GTPD_holder.value;
		rate_GTPD_GTP = rate_GTPD_GTP_holder.value;
		rate_GTPD_GDPPID = rate_GTPD_GDPPID_holder.value;
		rate_GDPPID_GDP = rate_GDPPID_GDP_holder.value;
		initialStateGTP = initialStateGTP_holder.value;
		neighborSearchStep = neighborSearchStep_holder.value;
		neighborSearchDistance = neighborSearchDistance_holder.value;
		membraneDiskThickness = membraneDiskThickness_holder.value;
		GTP_concentration = GTP_concentration_holder.value;
		GDP_concentration = GDP_concentration_holder.value;
		outputfile = outputfile_holder.value;
		inputfile = inputfile_holder.value;
		viewerOn = viewerOn_holder.value;
		useCodeForInput = useCodeForInput_holder.value;
		variableTimestepOn = variableTimestepOn_holder.value;
		output_torque = output_torque_holder.value;
		output_torque_step = output_torque_step_holder.value;
		maximumSqDisplacement = maximumSqDisplacement_holder.value;
		
		//********** Error handling for required parameters **********
		if(steps < 0) { Simulation.quit(Simulation.NEG_STEPS); }
		if(outputStep < 0) { Simulation.quit(Simulation.NEG_OUTPUTSTEP); }
		//************************************************************
		//********** Other situational error handling ****************
		if(inputfile_holder.value.equals("NO_FILENAME_SET") && !useCodeForInput) { Simulation.quit(Simulation.NO_INPUTFILE); }
		//************************************************************
		//********** Flag values *************************************
		if(random_seed > 0) { 
			Simulation.random = new Random(Param.random_seed);
			System.out.println("Using user provided random seed: "+Param.random_seed);
		} else { 
			Simulation.random = new Random(startTime);
			System.out.println("Using random seed: "+ startTime);
		}
		
		
		//1. scale energies by kT (boltzmann constant since temp is 1) and lengths
		kT = temperature * boltzmann_constant;
		kT_unit = boltzmann_constant; //means kT at 310 K in reduced units
		stretching_modulus = stretching_modulus * kT_unit;
		gauss_modulus = gauss_modulus * kT_unit * stalkDimerSeparation;
		curvature_modulus = curvature_modulus * kT_unit * stalkDimerSeparation;		
		twist_modulus = twist_modulus * kT_unit * stalkDimerSeparation;
		membraneFilament_stiffness = membraneFilament_stiffness * kT_unit;
		membrane_tension = membrane_tension * kT_unit * membraneDiskThickness;
		membrane_stiffness = membrane_stiffness * kT_unit * membraneDiskThickness;
		repulsionStiffness = repulsionStiffness * kT_unit;
		mmDimer_constantForce = mmDimer_constantForce * kT_unit;
		//2. scale on rates
		rate_APO_GTP = rate_APO_GTP * GTP_concentration;
		rate_APO_GDP = rate_APO_GDP * GDP_concentration;
		//3. other things
		mobility[0] = membrane2DMobility; //beads are embedded in the membrane
		mobility[1] = membrane2DMobility;
		mobility[2] = membrane2DMobility;
		rf_coeff[0] = Math.sqrt(2.0*kT*mobility[0]/timestep); //coefficient for random forces
		rf_coeff[1] = Math.sqrt(2.0*kT*mobility[1]/timestep);
		rf_coeff[2] = Math.sqrt(2.0*kT*mobility[2]/timestep);
	}
	
	

	

		
}
package noel.dynamin;
import java.util.*;
import noel.util.math.*;

//issues
//1. isDimerizable does not consider if a full turn of the filament is in the way. Somehow need to include the nearest neighbor bonds in the crossing calculation. Right now, have to keep excluded volume large enough so dimer cannot be made across two turns.

class NeighborList {
	
	public Membrane membrane; //the membrane template the filaments are attached to
	public Vector<Filament> filamentList;
	public double distance; //max distance of neighbor

	//structure of list: [ diskIndex, list of neighbors ]
	public ArrayList<ArrayList> list; //raw distance list
	//structure of list: [ repulsionIndex, [Bead i, Bead j] ]
	public ArrayList<ArrayList> repulsionList; //trimmed for repulsion calculation
	//structure of index: [ filamentIndex, beadIndex, moduleIndex, true: dimerizable ]
	public boolean[][][] dimerizableIndex;
	//structure of index: [ filamentIndex, beadIndex, moduleIndex, MotorModule partner]
	//partner list is null if non-existent
	public ArrayList[][][] dimerizablePartners; //base ArrayList should contain MotorModules
	//structure of index: [ filamentIndex, filamentIndex, beadIndex, beadIndex, true: dimerized ]
	public boolean[][][][] dimerIndex;
	//structure of list: [ repulsionIndex, [MotorModule i, MotorModule j] ]
	public ArrayList<ArrayList> dimerList; //trimmed for dimer interaction calculation
	
	public NeighborList(Membrane membrane, Vector<Filament> filamentList, double dist) {
		this.membrane = membrane;
		this.filamentList = filamentList;
		this.distance = dist;
		dimerList = new ArrayList<ArrayList>();
		//initialize the dimerIndex with falses, i.e. no Gdimers formed
		dimerIndex = new boolean[filamentList.size()][filamentList.size()][][];
		for(int i = 0; i < filamentList.size(); i++) {
			for(int j = 0; j < filamentList.size(); j++) {
				Vector<Bead> beads_i = filamentList.elementAt(i).beads;
				Vector<Bead> beads_j = filamentList.elementAt(j).beads;
				dimerIndex[i][j] = new boolean[beads_i.size()][beads_j.size()];
				for(int k = 0; k < beads_i.size(); k++) {
					for(int l = 0; l < beads_j.size(); l++) {
						dimerIndex[i][j][k][l] = false;
					}
				}
			}
		}
	}
	
	public void updateNeighborList() {
		//initialize list
		ArrayList<ArrayList> shortList = new ArrayList<ArrayList>(membrane.disks.size());
		for(int i = 0; i < membrane.disks.size(); i++) {
			shortList.add(new ArrayList<Bead>());
		}
		//loop over all beads, adding each to the appropriate "disk" in the shortList
		for(int i = 0; i < filamentList.size(); i++) {
			Vector<Bead> beads = filamentList.elementAt(i).beads;
			for(int j = 0; j < beads.size(); j++) {
				Bead bead = beads.elementAt(j);
				int diskIndex = Simulation.membrane.getDiskIndex(bead.z);
				bead.underlyingMembraneDisk = diskIndex;
				shortList.get(diskIndex).add(bead);
			}
		}
		//make full list considering distance (i.e. concatenating several disks' lists)
		list = new ArrayList<ArrayList>(membrane.disks.size());
		for(int i = 0; i < membrane.disks.size(); i++) {
			list.add(new ArrayList<Bead>());
			int bufferSize = ((int) (distance/Param.membraneDiskThickness)) + 1;
			int start = i - bufferSize;
			if(start<0) start=0;
			int end = i + bufferSize;
			if(end>=membrane.disks.size()) end = membrane.disks.size() - 1;
			for(int j = start; j <= end; j++) {
				list.get(i).addAll((ArrayList)shortList.get(j));
			}
		}
		updateRepulsionList();
		updateDimerizableIndex();
	}
	
	/**
	* Refines the distance based list of updateNeighborList() to only include the repulsion neighbors:
	* 1. separation less than Param.repulsionCutoff
	* 2. not close in sequence, i.e. |i-j|>4
	*/
	private void updateRepulsionList() {		
		repulsionList = new ArrayList<ArrayList>();
		for(int i = 0; i < filamentList.size(); i++) {
			Vector<Bead> beads = filamentList.elementAt(i).beads;
			for(int j = 0; j < beads.size(); j++) {
				Bead bead = beads.elementAt(j);
				ArrayList<Bead> neighbors = getNeighbors(bead);
				for(int k = 0; k < neighbors.size(); k++) {
					Bead neigh = neighbors.get(k);
					if(bead.z > neigh.z) { //only count each pair once (avoid double counting)
						if(neigh.filamentIndex == bead.filamentIndex) { //same filament
							int diff = Math.abs(bead.index-neigh.index);
							if(diff > 4) { //no neighbors nearby in sequence
								double distance = Coordinates.distance(bead.coord,neigh.coord);
								if(distance < Param.repulsionCutoff) {
									//System.out.println(i+" "+j+" "+k+" "+bead.filamentIndex+" "+bead.index+" "+neigh.filamentIndex+" "+neigh.index);
									ArrayList<Bead> newRepulsion = new ArrayList<Bead>(2);
									newRepulsion.add(bead);
									newRepulsion.add(neigh);
									repulsionList.add(newRepulsion);
								}
							}
						} else { //different filaments
							double distance = Coordinates.distance(bead.coord,neigh.coord);
							if(distance < Param.repulsionCutoff) {
								ArrayList<Bead> newRepulsion = new ArrayList<Bead>(2);
								newRepulsion.add(bead);
								newRepulsion.add(neigh);
								repulsionList.add(newRepulsion);								
							}
						}
					}
				}
			}
		}
	}
	
	
	public void updateDimerizableIndex() {
		ArrayList<MotorModule> possibles;
		//initialize index with null
		dimerizableIndex = new boolean[filamentList.size()][][];
		dimerizablePartners = new ArrayList[filamentList.size()][][];
		for(int i = 0; i < filamentList.size(); i++) {
			Vector<Bead> beads = filamentList.elementAt(i).beads;
			dimerizableIndex[i] = new boolean[beads.size()][2];
			dimerizablePartners[i] = new ArrayList[beads.size()][2];
			for(int j = 0; j < beads.size(); j++) {
				dimerizableIndex[i][j][Param.UP] = false;
				dimerizableIndex[i][j][Param.DOWN] = false;
				dimerizablePartners[i][j][Param.UP] = null;
				dimerizablePartners[i][j][Param.DOWN] = null;
			}
		}
		
		for(int i = 0; i < filamentList.size(); i++) {
			Vector<Bead> beads = filamentList.elementAt(i).beads;
			for(int j = 0; j < beads.size(); j++) {
				Bead bead = beads.elementAt(j);
				MotorModule up = bead.modules[Param.UP]; //can check all ups, downs will be found as partners
				if( ! up.isDimerized) { //check not already partnered
					//check the neighbors for possible partners
					ArrayList<Bead> neighbors = getNeighbors(bead);
					for(int k = 0; k < neighbors.size(); k++) {
						Bead neigh = neighbors.get(k);
						MotorModule down = neigh.modules[Param.DOWN];						
						if( ! down.isDimerized) { //neighbor DOWN MM shouldn't be dimerized							
							//down neighbor should be at larger z since checking relative to an up
							if(up.z < down.z) { //z of UP < z of DOWN									
								//also for right handed on same filament down.index > up.index and should be further apart than 4 dimers along the chain
								if(up.myBead.filamentIndex != down.myBead.filamentIndex || 
								(up.myBead.filamentIndex == down.myBead.filamentIndex) && (down.myBead.index - up.myBead.index) > 4 ) {									
									double vdist = Coordinates.distance(up.getCoords(),down.getCoords());									
									double vdphi = up.getDeltaPhi(down);
									
									if(vdphi > 0 && vdist < Param.mmDimer_maxLength) {										
										//geometry is ok, check for blocking links
										//for now blocking links are only checked as those belonging to the
										//same filaments as up/down. This ignore possible blockers created
										//where two filament ends meet, but this scenario is not relevant yet
										int numBlocker = 0, s, e;
										int n = 2; //check 2 either side
										int upFilIndex = up.myBead.filamentIndex;
										int downFilIndex = down.myBead.filamentIndex;
										s = up.myBead.index - n;
										if(s<0) s = 0;
										e = down.myBead.index + n;
										if(e>filamentList.elementAt(down.myBead.filamentIndex).beads.size()) 
											e=filamentList.elementAt(down.myBead.filamentIndex).beads.size();
										for(int a = s; a < up.myBead.index; a++) {
											for(int b = down.myBead.index + 1; b < e; b++) {
												if(isDimerized(upFilIndex,downFilIndex,a,b)) numBlocker++;
											}
										}
										s = down.myBead.index - n;
										if(s<0) s = 0;
										e = up.myBead.index + n;
										if(e>filamentList.elementAt(up.myBead.filamentIndex).beads.size()) 
											e=filamentList.elementAt(up.myBead.filamentIndex).beads.size();
										for(int a = s; a < down.myBead.index; a++) {
											for(int b = up.myBead.index + 1; b < e; b++) {
												if(isDimerized(downFilIndex,upFilIndex,a,b)) numBlocker++;
											}
										}
										if(numBlocker == 0) { 
											int a = up.myBead.index;
											int b = down.myBead.index;
											//System.out.println("Possible neighbors: "+a+" "+b); 
											dimerizableIndex[upFilIndex][a][Param.UP] = true;
											dimerizableIndex[downFilIndex][b][Param.DOWN] = true;
											if(dimerizablePartners[upFilIndex][a][Param.UP] == null) {
												dimerizablePartners[upFilIndex][a][Param.UP] = new ArrayList<MotorModule>();
											}
											if(dimerizablePartners[downFilIndex][b][Param.DOWN] == null) {
												dimerizablePartners[downFilIndex][b][Param.DOWN] = new ArrayList<MotorModule>();
											}
											dimerizablePartners[upFilIndex][a][Param.UP].add(down);
											dimerizablePartners[downFilIndex][b][Param.DOWN].add(up);
										}
									}
								}
							}
						}
					}
				}
			}
		}		
	}
	
	public void addDimer(MotorModule a, MotorModule b) {
		a.isDimerized = true;
		a.myPartner = b;
		b.isDimerized = true;
		b.myPartner = a;
		dimerIndex[a.myBead.filamentIndex][b.myBead.filamentIndex][a.myBead.index][b.myBead.index] = true;
		ArrayList<MotorModule> dimer = new ArrayList<MotorModule>(2);
		dimer.add(a);
		dimer.add(b);
		dimerList.add(dimer);
		updateDimerizableIndex();
		//System.out.println("Dimer made between:\n"+a+"\n"+b);
	}
	
	
	public void removeDimer(MotorModule a, MotorModule b) {
		a.isDimerized = false;
		a.myPartner = null;
		b.isDimerized = false;
		b.myPartner = null;
		dimerIndex[a.myBead.filamentIndex][b.myBead.filamentIndex][a.myBead.index][b.myBead.index] = false;
		dimerIndex[b.myBead.filamentIndex][a.myBead.filamentIndex][b.myBead.index][a.myBead.index] = false;
		for(int i = 0; i < dimerList.size(); i++) {
			MotorModule a1 = (MotorModule)dimerList.get(i).get(0);
			MotorModule b1 = (MotorModule)dimerList.get(i).get(1);
			if(a.myBead.filamentIndex == a1.myBead.filamentIndex && a.myBead.index == a1.myBead.index && b.myBead.filamentIndex == b1.myBead.filamentIndex && b.myBead.index == b1.myBead.index ) {
				dimerList.remove(i);
				break;
			}
			if(b.myBead.filamentIndex == a1.myBead.filamentIndex && b.myBead.index == a1.myBead.index && a.myBead.filamentIndex == a1.myBead.filamentIndex && b.myBead.index == b1.myBead.index ) {
				dimerList.remove(i);
				break;
			}
		}
		updateDimerizableIndex();
	}
	
	public boolean isDimerizable(int filIndex, int beadIndex, int mmIndex) {
		return dimerizableIndex[filIndex][beadIndex][mmIndex];
	}
	
	public boolean isDimerized(int filIndexA, int filIndexB, int beadA, int beadB) {
		if(dimerIndex[filIndexA][filIndexB][beadA][beadB] || dimerIndex[filIndexB][filIndexA][beadB][beadA]) return true;
		else return false;
	}
	
	public ArrayList<Bead> getNeighbors(Bead bead) {
		return list.get(bead.underlyingMembraneDisk);
	}
}
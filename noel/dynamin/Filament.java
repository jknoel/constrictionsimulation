package noel.dynamin;
import noel.util.math.*;
import java.util.*;

class Filament {
	
	Vector<Bead> beads;
	Membrane membrane; //the associated membrane tube
	int length;
	double curvature_modulus;
	double twist_modulus;
	double gauss_modulus;
	double stretching_modulus;
	double stalkDimerSeparation;
	int index; //specifies which filament this is (i.e. its name)
		
	public Filament(int index, double curvature_modulus, double twist_modulus, double stretching_modulus, double stalkDimerSeparation, double gauss_modulus) {
		length = 0;
		this.curvature_modulus = curvature_modulus;
		this.twist_modulus = twist_modulus;
		this.stretching_modulus = stretching_modulus;
		this.stalkDimerSeparation = stalkDimerSeparation;
		this.gauss_modulus = gauss_modulus;
		this.index = index;
		beads = new Vector();
	}
	
	public void setMembraneTube(Membrane membrane) {
		this.membrane = membrane;
	}
	
	public int add(Bead bead) {
		beads.add(bead);
		if(length>0) beads.elementAt(length-1).setNeighbor(bead);
		length++;
		return beads.size();
	}
	
	public void update() {
		if(length<3) { System.out.println("Filament too short!"); System.exit(1);}
		for(int i=0;i<length;i++) { beads.elementAt(i).update(); }
		for(int i=0;i<length-1;i++) { beads.elementAt(i).update2(); }
	}
	
	public void updateNeighborLists() {
		for(int i=0;i<length;i++) { beads.elementAt(i).updateMembraneDisk(); }
	}
	
	public void applyBonds() {
		double energy = 0;
		for(int j = 0; j < beads.size()-1; j++) {
			Bead bead = beads.elementAt(j);
			Bead bead_p = beads.elementAt(j+1);
			double distance = Coordinates.distance(bead.coord,bead_p.coord);
			double diffx = Coordinates.diffx(bead.coord,bead_p.coord);
			double diffy = Coordinates.diffy(bead.coord,bead_p.coord);
			double diffz = Coordinates.diffz(bead.coord,bead_p.coord);
			double fx = - stretching_modulus * ( distance - stalkDimerSeparation ) * diffx / distance;
			//System.out.println(bead.distance+" "+j);
			bead.fx += fx;
			bead_p.fx += - fx;
			double fy = - stretching_modulus * ( distance - stalkDimerSeparation ) * diffy / distance;
			bead.fy += fy;
			bead_p.fy += -fy;
			double fz = - stretching_modulus * ( distance - stalkDimerSeparation ) * diffz / distance;
			bead.fz += fz;
			bead_p.fz += -fz;
			energy += stretching_modulus * ( distance - stalkDimerSeparation ) * ( distance - stalkDimerSeparation );
		}
		//System.exit(0);
		Logger.bondE = energy;
	}
	
	public void applyCurvature() {
		double energy = 0;
		for(int j = 1; j < beads.size()-1; j++) { 
			Bead bead = beads.elementAt(j);
			Bead bead_m = beads.elementAt(j-1);
			energy += curvature_modulus * bead.kappaDiff * bead.kappaDiff;
			double force = - bead.kappaDiff * (-bead.A);
			bead.rf += curvature_modulus*force;
			force = - (bead_m.kappaDiff * bead_m.B * (bead_m.dpinv) - bead.kappaDiff * bead.B * (bead.dpinv));
			bead.zf += curvature_modulus * force;
			force = - bead.rinv *  (-bead_m.kappaDiff * bead_m.B * (bead_m.dhdp) + bead.kappaDiff * bead.B * (bead.dhdp));
			bead.pf += curvature_modulus * force;
		}
		//end cases
		Bead bead = beads.elementAt(0);
		bead.rf += -curvature_modulus * bead.kappaDiff * (-bead.A);
		bead.zf += -curvature_modulus * (- bead.kappaDiff * bead.B * (bead.dpinv));
		bead.pf += -curvature_modulus * bead.rinv * ( bead.kappaDiff * bead.B * (bead.dhdp));
		energy += curvature_modulus * bead.kappaDiff * bead.kappaDiff;
		bead = beads.elementAt(beads.size()-1);
		Bead bead_m = beads.elementAt(beads.size()-2);
		bead.rf += -curvature_modulus * bead.kappaDiff * (-bead.A);
		bead.zf += -curvature_modulus * (bead_m.kappaDiff * bead_m.B * (bead_m.dpinv));
		bead.pf += -curvature_modulus * bead.rinv * (-bead_m.kappaDiff * bead_m.B * (bead_m.dhdp));
		
		Logger.curvE = energy;
	}
	
	public void applyTwist() {
		double energy = 0;
		for(int j = 1; j < beads.size()-1; j++) { 
			Bead bead = beads.elementAt(j);
			Bead bead_m = beads.elementAt(j-1);
			bead.zf += -twist_modulus * (-bead.tauDiff * bead.A * (bead.dpinv) + bead_m.tauDiff * bead_m.A * (bead_m.dpinv));
			bead.pf += -twist_modulus * bead.rinv * (bead.tauDiff * bead.A * (bead.dhdp) - bead_m.tauDiff * bead_m.A * (bead_m.dhdp));
			bead.rf += -twist_modulus * bead.tauDiff * bead.B;
			energy += twist_modulus * bead.tauDiff * bead.tauDiff;
		}
		//end cases
		Bead bead = beads.elementAt(0);
		bead.zf += -twist_modulus * -bead.tauDiff * bead.A * (bead.dpinv);
		bead.pf += -twist_modulus * bead.rinv * bead.tauDiff * bead.A * (bead.dhdp);
		energy += twist_modulus * bead.tauDiff * bead.tauDiff;
		bead = beads.elementAt(beads.size()-1);		
		Bead bead_m = beads.elementAt(beads.size()-2);
		bead.zf += -twist_modulus * bead_m.tauDiff * bead_m.A * (bead_m.dpinv);
		bead.pf += -twist_modulus * bead.rinv * (-bead_m.tauDiff * bead_m.A * (bead_m.dhdp));
		
		Logger.twistE = energy;
	}
	
	public void applyGeo() {
		double energy = 0;
		double distinv = 1.0 / stalkDimerSeparation;
		for(int j = 1; j < beads.size()-1; j++) { 
			Bead bead = beads.elementAt(j);
			Bead bead_m = beads.elementAt(j-1);
			bead.sigma = bead.r*(bead.tau-bead_m.tau)*distinv - bead.dzdp*(bead.kappa-bead_m.kappa)*distinv;		
			bead.dtaudxi = distinv * (bead.tau-bead_m.tau);
			bead.dkappadxi = distinv * (bead.kappa-bead_m.kappa);
			bead.dAdxi = distinv * (bead.A-bead_m.A);
			bead.dBdxi = distinv * (bead.B-bead_m.B);
			bead.dAdzdxi = distinv * (bead.A*bead.dhdz-bead_m.A*bead_m.dhdz);
			bead.dAdpdxi = distinv * (bead.A*bead.dhdp-bead_m.A*bead_m.dhdp);
			bead.dBdzdxi = distinv * (bead.B*bead.dhdz-bead_m.B*bead_m.dhdz);
			bead.dBdpdxi = distinv * (bead.B*bead.dhdp-bead_m.B*bead_m.dhdp);
		}
		beads.elementAt(0).sigma = 0;
		beads.elementAt(beads.size()-1).sigma = 0;
		double force,rf,zf,pf;
		for(int j = 1; j < beads.size()-1; j++) { 
			Bead bead = beads.elementAt(j);
			Bead bead_m = beads.elementAt(j-1);
			energy += gauss_modulus * bead.sigma * bead.sigma;
			rf = -gauss_modulus * bead.sigma * ( bead.dtaudxi + bead.r * bead.dBdxi + bead.dzdp * bead.dAdxi); 
			bead.rf += rf;
			force = -bead.sigma*bead.r*bead.dAdpdxi + bead_m.sigma*bead_m.r*bead_m.dAdpdxi  
				+ bead.sigma*bead.dhdp*bead.dkappadxi - bead_m.sigma*bead_m.dhdp*bead_m.dkappadxi
				+ bead.sigma*bead.dzdp*bead.dBdpdxi - bead_m.sigma*bead_m.dzdp*bead_m.dBdpdxi;
			pf = force * gauss_modulus * bead.rinv;
			bead.pf += pf;
			force = bead.sigma*bead.r*bead.dAdzdxi - bead_m.sigma*bead_m.r*bead_m.dAdzdxi  
				- bead.sigma*bead.dhdz*bead.dkappadxi + bead_m.sigma*bead_m.dhdz*bead_m.dkappadxi
				- bead.sigma*bead.dzdp*bead.dBdzdxi + bead_m.sigma*bead_m.dzdp*bead_m.dBdzdxi;
			zf = force * gauss_modulus; 
			bead.zf += zf;
			if(Param.forceLogging && Simulation.step % Param.forceLoggingRate == 0) {
				double[] f = {rf,pf,0,0,zf};
				Logger.logs[Logger.GEO].logMaxForce(j,f);
				rf = 0; pf = 0; zf = 0;
			}			
		}
		//end case
		Bead bead = beads.elementAt(beads.size()-1);
		Bead bead_m = beads.elementAt(beads.size()-2);
		force = bead_m.sigma*bead_m.r*bead_m.dAdpdxi  
			- bead_m.sigma*bead_m.dhdp*bead_m.dkappadxi - bead_m.sigma*bead_m.dzdp*bead_m.dBdpdxi;
		pf = force * gauss_modulus * bead.rinv;
        bead.pf += pf;
        force = - bead_m.sigma*bead_m.r*bead_m.dAdzdxi + 
			+ bead_m.sigma*bead_m.dhdz*bead_m.dkappadxi + bead_m.sigma*bead_m.dzdp*bead_m.dBdzdxi;
		zf = force * gauss_modulus;
        bead.zf += zf;
		if(Param.forceLogging && Simulation.step % Param.forceLoggingRate == 0) {
			double[] f = {0,pf,0,0,zf};
			Logger.logs[Logger.GEO].logMaxForce(beads.size()-1,f);
			rf = 0; pf = 0; zf = 0;
		}
		Logger.geoE = energy;
	}

	static void applyBeadRepulsion(NeighborList neighList) {
		double energy = 0;
		ArrayList<ArrayList> repulsionList = neighList.repulsionList;
		for(int i = 0; i < repulsionList.size(); i++) {
			Bead bead1 = (Bead) repulsionList.get(i).get(0);
			Bead bead2 = (Bead) repulsionList.get(i).get(1);
			double distance = Coordinates.distance(bead1.coord,bead2.coord);
			energy += Filament.applyBeadRepulsionSingle(bead1,bead2,distance);
		}
		Logger.repE = energy;
	}
		
	// E(dist) = repulsionStiffness/2 * (dist-Param.repulsionCutoff)^2
	static double applyBeadRepulsionSingle(Bead a, Bead b, double dist) {
		double dx = Coordinates.diffx(a.coord,b.coord); //a-b
		double dy = Coordinates.diffy(a.coord,b.coord);
		double dz = Coordinates.diffz(a.coord,b.coord);
		double diff;
		
		if(dist < Param.repulsionCutoff) diff = Param.repulsionCutoff - dist;
		else return 0;
		
		double force = Param.repulsionStiffness * diff / dist;
		a.fx += force*dx;
		a.fy += force*dy;
		a.fz += force*dz;
		b.fx -= force*dx;
		b.fy -= force*dy;
		b.fz -= force*dz;
		
		return Param.repulsionStiffness * diff * diff;
	}
	
	static void applyDimerInteraction(NeighborList neighList) {
		double energy = 0;
		ArrayList<ArrayList> dimerList = neighList.dimerList;
		for(int i = 0; i < dimerList.size(); i++) {
			MotorModule mm1 = (MotorModule) dimerList.get(i).get(0);
			MotorModule mm2 = (MotorModule) dimerList.get(i).get(1);
			if(mm1.nucleotideState == Param.GDPPID && mm2.nucleotideState == Param.GDPPID) {
				energy += Filament.applyDimerInteractionSingle(mm1,mm2);
			}
		}
		//energy not well defined for the linear ramp potential...
	}
	
	//Implemented here as a constant force
	//E(dist) = Param.mmDimer_constantForce * dist;
	static double applyDimerInteractionSingle(MotorModule mm1, MotorModule mm2) {
		double dx = Coordinates.diffx(mm1.getCoords(),mm2.getCoords()); //a-b
		double dy = Coordinates.diffy(mm1.getCoords(),mm2.getCoords());
		double dz = Coordinates.diffz(mm1.getCoords(),mm2.getCoords());
		double distance = Coordinates.distance(mm1.getCoords(),mm2.getCoords());
		if(distance < Param.mmDimer_zeroForceLength) return 0;
		double force = -Param.mmDimer_constantForce / distance;
		Bead a = mm1.myBead;
		Bead b = mm2.myBead;
		a.fx += force*dx;
		a.fy += force*dy;
		a.fz += force*dz;
		b.fx -= force*dx;
		b.fy -= force*dy;
		b.fz -= force*dz;
				
		if(Param.output_torque) { Logger.logMotorTorque(a,b,force,dx,dy,dz); }
		
		return 0; //energy is not well defined for this potential
	}
	
	static void makeStateChanges(Vector<Filament> filamentList, NeighborList list) {
		
		for(int i = 0; i < filamentList.size(); i++) { 
			Vector<Bead> beads = filamentList.elementAt(i).beads;
			for(int j = 0; j < beads.size(); j++) {
				Bead bead = beads.elementAt(j);
				for(int k = 0; k < 2; k++) { //for each motor module
					MotorModule me = bead.modules[k];
					switch (me.nucleotideState) {
						case Param.APO: 
							//GTP binds
							if(Simulation.isMarkovMoveTaken(Param.rate_APO_GTP,Param.timestep)) me.setNucleotideState(Param.GTP);
							//GDP binds
							else if(Simulation.isMarkovMoveTaken(Param.rate_APO_GDP,Param.timestep)) me.setNucleotideState(Param.GDP);
							break;
						case Param.GDP: 
							//GDP falls off
							if(Simulation.isMarkovMoveTaken(Param.rate_GDP_APO,Param.timestep)) me.setNucleotideState(Param.APO);
							break;
						case Param.GTP: 				
							if(Simulation.isMarkovMoveTaken(Param.rate_GTP_APO,Param.timestep)) { //GTP falls off
								me.setNucleotideState(Param.APO);
								break;
							}
							if(Simulation.isMarkovMoveTaken(Param.rate_GTP_GTPD,Param.timestep)) { //dimerization could happen								
								if(k==Param.UP && list.isDimerizable(i,j,k)) { //UP MM and neighbor is available! (only UP to avoid double counting)
									
									int numPossiblePartners = list.dimerizablePartners[i][j][k].size();
									MotorModule partner = (MotorModule)list.dimerizablePartners[i][j][k].get(Simulation.random.nextInt(numPossiblePartners));
									list.addDimer(me,partner);
									me.setNucleotideState(Param.GTPD);
									partner.setNucleotideState(Param.GTPD);
								}
							}
							break;
						case Param.GTPD:
							if(k==Param.UP) { //for double counting purposes
								if(Simulation.isMarkovMoveTaken(Param.rate_GTPD_GTP,Param.timestep)) { //spontaneous dissociation
									me.myPartner.setNucleotideState(Param.GTP);
									me.setNucleotideState(Param.GTP);
									list.removeDimer(me,me.myPartner);
								}
								else if(Simulation.isMarkovMoveTaken(Param.rate_GTPD_GDPPID,Param.timestep)) {
									me.myPartner.setNucleotideState(Param.GDPPID);
									me.setNucleotideState(Param.GDPPID);
									Logger.incrementGTPburned();
								} else if(! me.isGTPDimerValid()) {
									me.myPartner.setNucleotideState(Param.GTP);
									me.setNucleotideState(Param.GTP);
									list.removeDimer(me,me.myPartner);
								}
							}
							break;
						case Param.GDPPID:
							if(k==Param.UP) { //for double counting purposes
								if(! me.isGDPPIDimerValid() && false) {
									me.myPartner.setNucleotideState(Param.GDP);
									me.setNucleotideState(Param.GDP);
									list.removeDimer(me,me.myPartner);
								}
								if(Simulation.isMarkovMoveTaken(Param.rate_GDPPID_GDP*me.getAllostericFactor(),Param.timestep)) {
									me.myPartner.setNucleotideState(Param.GDP);
									me.setNucleotideState(Param.GDP);
									list.removeDimer(me,me.myPartner);
								}
							}
							break;
					}
				}
			}
		}
		
	}

	
}
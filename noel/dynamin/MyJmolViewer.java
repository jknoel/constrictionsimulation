package noel.dynamin;


import java.awt.Container;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.JFrame;
import javax.swing.JPanel;
import org.jmol.adapter.smarter.SmarterJmolAdapter;
import org.jmol.api.JmolAdapter;
import org.jmol.api.JmolViewer;
import noel.util.math.*;
import java.util.*;

public class MyJmolViewer {

   public JmolViewer viewer;

   JmolPanel jmolPanel;
   JFrame frame;

   public MyJmolViewer() {
      initializeJmolPanel();
   }
   
   public void setFile(String s,NeighborList list) {
	   	getViewer().openFile(s);
		getViewer().evalString("rotate x 90;spacefill on");
		paintGDPPI(list);
   }
   
   public void initializeJmolPanel() {
       frame = new JFrame();
       frame.addWindowListener(new ApplicationCloser());
       Container contentPane = frame.getContentPane();
       jmolPanel = new JmolPanel();

       jmolPanel.setPreferredSize(new Dimension(400,400));
       contentPane.add(jmolPanel);

       frame.pack();
       frame.setVisible(true); 
   }
   
   public JmolViewer getViewer(){
       return jmolPanel.getViewer();
   }
   
   public void paintGDPPI(NeighborList neighList) {
	ArrayList<ArrayList> dimerList = neighList.dimerList;
	int count = 1;
	for(int i = 0; i < dimerList.size(); i++) {
		MotorModule mm1 = (MotorModule) dimerList.get(i).get(0);
		MotorModule mm2 = (MotorModule) dimerList.get(i).get(1);
		if(mm1.nucleotideState == Param.GDPPID && mm2.nucleotideState == Param.GDPPID) {
			double distance = Coordinates.distance(mm1.getCoords(),mm2.getCoords());
			int j = mm1.myBead.index+1;
			int k = mm2.myBead.index+1;
			getViewer().evalString("draw line"+count+" "+distance+" (atomno="+j+") (atomno="+k+");color $line"+count+" yellow");
			count++;
		}
	}
   }
static void applyDimerInteraction(NeighborList neighList) {
	double energy = 0;
	ArrayList<ArrayList> dimerList = neighList.dimerList;
	for(int i = 0; i < dimerList.size(); i++) {
		MotorModule mm1 = (MotorModule) dimerList.get(i).get(0);
		MotorModule mm2 = (MotorModule) dimerList.get(i).get(1);
		if(mm1.nucleotideState == Param.GDPPID && mm2.nucleotideState == Param.GDPPID) {
			energy += Filament.applyDimerInteractionSingle(mm1,mm2);
		}
	}
	//energy not well defined for the linear ramp potential...
}
   
   
   static class ApplicationCloser extends WindowAdapter {
       public void windowClosing(WindowEvent e) {
           System.exit(0);
       }
   }

   static class JmolPanel extends JPanel {
       JmolViewer viewer;
       JmolAdapter adapter;
       JmolPanel() {
           adapter = new SmarterJmolAdapter();
           viewer = JmolViewer.allocateViewer(this, adapter);

       }

       public JmolViewer getViewer() {
           return viewer;
       }

       public void executeCmd(String rasmolScript){
           viewer.evalString(rasmolScript);
       }


       final Dimension currentSize = new Dimension();
       final Rectangle rectClip = new Rectangle();

       public void paint(Graphics g) {
           getSize(currentSize);
           g.getClipBounds(rectClip);
           viewer.renderScreenImage(g, currentSize, rectClip);
       }
   }
}
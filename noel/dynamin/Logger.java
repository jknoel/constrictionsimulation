package noel.dynamin;

import java.util.*;
import noel.io.*;
import noel.util.math.*;

public class Logger {
	//force ids
	final static int CURV = 0;
	final static int TWIST = 1;
	final static int GEO = 2;
	final static int REP = 3;
	final static int INT = 4;				
	final static int HELF = 5;
	final static int NUMFORCES = 6;
	//time ids
	final static int UPDATE_COORDINATES = 0;
	final static int NEIGHBOR_SEARCH = 1;
	final static int FILAMENT_ELASTIC = 2;
	final static int MEMBRANE_ELASTIC = 3;
	final static int REPULSION = 4;
	final static int MM_DIMER_INTERACTION = 5;
	final static int INTEGRATE = 6;
	final static int MARKOV_STATES = 7;
	final static int NUM_TIMING = 8;
	
	static long currentStep = -1;
	static FileIO out, motorTorqueOut, totalTorqueOut;
	static long[] timeKeeper = new long[NUM_TIMING];
	static Logger[] logs;
	static int gtpUsed; //number of GTP molecules hydrolyzed
	static long lastBeginTime;
	
	public static Vector<Filament> filamentList;
	
	//torque output variables
	static boolean printTorqueInit = false;
	static double[][] motorTorqueSum = null;
	static double[][] totalTorqueSum = null;
	static double nextTime = 0;
	
	//Quantities that may be needed for energy output
	public static double bondE = 0,curvE = 0,twistE = 0,geoE = 0,memE = 0, intE = 0, repE = 0;
	
	//instance variables
	int forceType;
	String nextText;
	int bufferCount = 0;
	double tempMaxF = -1;
	int tempMaxIndex = -1;
	
	public Logger(int forceType) {
		nextText="";
		this.forceType = forceType;
	}
	
	public void logForce(int index, double[] forces) {
		if(Simulation.step != currentStep) {
			currentStep = Simulation.step;
			out.write(nextText);
			nextText = "";
		}
		nextText += String.format("%d %d %8.4f %8.4f %8.4f %8.4f %8.4f \n",forceType,index,forces[0],forces[1],forces[2],forces[3],forces[4]);
	}
	public void logMaxForce(int index, double[] forces) {
		if(Simulation.step != currentStep) {
			currentStep = Simulation.step;
			nextText += String.format("max %d %d %d %8.4f %f\n",currentStep,forceType,tempMaxIndex,tempMaxF,Integrator.timestep);
			bufferCount++;
			if(bufferCount%10==0) {out.write(nextText);nextText = "";}
			tempMaxF = -1; 
			tempMaxIndex = -1;
		}
		for(int i = 0; i < 5; i++) { if(Math.abs(forces[i]) > tempMaxF) { tempMaxF = Math.abs(forces[i]); tempMaxIndex = index;} }
	}
		
	public static void initialize() {
		for(int i = 0; i < NUM_TIMING; i++) { timeKeeper[i]=0; }
		logs = new Logger[NUMFORCES];
		if(Param.forceLogging) {
			out = new FileIO(Param.forceLoggingFile,FileIO.WRITING);
			logs[Logger.GEO] = new Logger(Logger.GEO);
		}
		if(Param.output_torque) {
			motorTorqueOut = new FileIO("motorTorque",FileIO.WRITING);
			totalTorqueOut = new FileIO("totalTorque",FileIO.WRITING);
		}
		gtpUsed = 0;
	}
	
	/**
	* Used for timing. Sets the start of the timing.
	*/
	public static void begin() {
		lastBeginTime = System.nanoTime();
	}
	/**
	* Used for timing. Ends the timing of the designated method by subtracting current time from <code>lastBeginTime</code>.
	*/
	public static void end(int methodID) {
		timeKeeper[methodID]+=System.nanoTime()-lastBeginTime;
	}
	
	public static void incrementGTPburned() {
		gtpUsed+=2;
	}
	
	public static void printTimeKeeper(long start) {
		double total = (double) (System.nanoTime() - start);
		System.out.println("Timing information: (total ms) (percentage of total time)");
		for(int i = 0; i < NUM_TIMING; i++) {
			System.out.println(String.format("%s: %d || %4.2f%%",
				timeKeeperName(i),((long)(timeKeeper[i]/1000000)),timeKeeper[i]/total*100));
		}
	}
	private static String timeKeeperName(int id) {
		switch (id) {
			case 0: return "UPDATE_COORDINATES";
			case 1: return "NEIGHBOR_SEARCH"; 
			case 2: return "FILAMENT_ELASTIC"; 
			case 3: return "MEMBRANE_ELASTIC"; 
			case 4: return "REPULSION"; 
			case 5: return "MM_DIMER_INTERACTION"; 
			case 6: return "INTEGRATE"; 
			case 7: return "MARKOV_STATES";
			default: return "UNKNOWN TIME EVENT";
		}
	}
	
	public static void printRepulsionNeighbors(NeighborList list) {
		ArrayList<ArrayList> repulsionList = list.repulsionList;
		for(int i = 0; i < repulsionList.size(); i++) {
			Bead bead1 = (Bead) repulsionList.get(i).get(0);
			Bead bead2 = (Bead) repulsionList.get(i).get(1);
			System.out.println(bead1+" "+bead2+" out distance is: "+Coordinates.distance(bead1.coord,bead2.coord));
		}
	}
	
	public static void printE() {
		double totalE = bondE + curvE + twistE + geoE + memE + intE + repE;
		System.out.printf("Step: %d time: %8.3f ms timestep %1.6f Total: %2.6f bond: %2.6f curv: %2.6f twist: %2.6f geo: %2.6f mem: %2.6f intE: %2.6f repE: %2.6f\n"
			,Simulation.step,Integrator.timeElapsed/1000, Integrator.timestep,totalE,bondE,curvE,twistE,geoE,memE,intE,repE);
		if((new Double(totalE)).isNaN()) { Simulation.quit("Something wrong, the energy is NaN."); }
	}
	public static void printFlow(Membrane mem) {
		for(int i = 0; i<mem.disks.size(); i++) {
			System.out.println(i+" "+mem.disks.elementAt(i).r+" "+mem.disks.elementAt(i).flowVelocity+" "+mem.disks.elementAt(i).rf
				+" "+mem.disks.elementAt(i).effectiveForceTimesMobility+" "+(-Math.PI * Param.membrane_stiffness * mem.disks.elementAt(i).r2inv)+" "
					+(2 * Math.PI * Param.membrane_tension));
		}
	}
	public static void printElastic(Filament fil) {
		for(int i = 0; i<fil.beads.size(); i++) {
			Bead bead = fil.beads.elementAt(i);
			System.out.printf("%d kappa: %2.6f tau: %2.6f geo: %2.6f rf: %2.6f pf: %2.6f zf: %2.6f\n",i,bead.kappa,bead.tau,bead.sigma,bead.rf,bead.pf,bead.zf);
		}
	}
	public static void printMinR(Membrane mem) {
		double smallest = 1000;
		for(int i = 0; i<mem.disks.size(); i++) {
			if(mem.disks.elementAt(i).r < smallest) smallest = mem.disks.elementAt(i).r;
		}
		System.out.printf("minr: %2.4f\n",smallest);
	}
	
	public static void printStates(Vector<Filament> filamentList) {
		for(int i = 0; i < filamentList.size(); i++) { 
			Vector<Bead> beads = filamentList.elementAt(i).beads;
			for(int j = 0; j < beads.size(); j++) {
				Bead bead = beads.elementAt(j);
				for(int k = 0; k < 2; k++) { //for each motor module
					switch (bead.modules[k].nucleotideState) {
						case Param.APO: System.out.println(i+" "+j+" "+k+" is APO"); break;
						case Param.GDP: System.out.println(i+" "+j+" "+k+" is GDP"); break;
						case Param.GTP: System.out.println(i+" "+j+" "+k+" is GTP"); break;
						case Param.GTPD: System.out.println(i+" "+j+" "+k+" is GTPD and my partner is "+bead.modules[k].myPartner.myBead.filamentIndex+" "+bead.modules[k].myPartner.myBead.index);	break;
						case Param.GDPPID: System.out.println(i+" "+j+" "+k+" is GDPPID and my parter is "+bead.modules[k].myPartner.myBead.filamentIndex+" "+bead.modules[k].myPartner.myBead.index);	break;
						default: System.out.println(i+" "+j+" "+k+" is UNKNOWN STATE");									
					}
				}
			}
		}
	}
	
	public static void logMotorTorque(Bead a, Bead b, double force, double dx, double dy, double dz) {
		if(motorTorqueSum == null) {
			int numFilament = filamentList.size();
			motorTorqueSum = new double[numFilament][];
			for(int i = 0; i < filamentList.size(); i++) { 
				int filLength = filamentList.elementAt(i).length;
				motorTorqueSum[i] = new double[filLength];
				for(int j = 0; j < filLength; j++){ motorTorqueSum[i][j] = 0; }
			}
			//nextTime += Param.output_torque_step;
		}
		motorTorqueSum[a.filamentIndex][a.index] += a.r * force*( -dx*Math.sin(a.p) + dy*Math.cos(a.p)) * Integrator.timestep;
		motorTorqueSum[b.filamentIndex][b.index] += -b.r * force*( -dx*Math.sin(b.p) + dy*Math.cos(b.p)) * Integrator.timestep;
		// if(Integrator.timeElapsed/1000 > nextTime) {
		// 	motorTorqueOut.write("step "+nextTime+"\n");
		// 	int numFilament = filamentList.size();
		// 	for(int i = 0; i < filamentList.size(); i++) {
		// 		int filLength = filamentList.elementAt(i).length;
		// 		for(int j = 0; j < filLength; j++){
		// 			motorTorqueOut.write(i+" "+j+" "+(motorTorqueSum[i][j]/(Param.output_torque_step*1000))+"\n");
		// 		}
		// 	}
		// 	motorTorqueSum = null;
		// }
	}
	public static void logTotalTorque(Bead a) {
		if(totalTorqueSum == null) {
			int numFilament = filamentList.size();
			totalTorqueSum = new double[numFilament][];
			for(int i = 0; i < filamentList.size(); i++) { 
				int filLength = filamentList.elementAt(i).length;
				totalTorqueSum[i] = new double[filLength];
				for(int j = 0; j < filLength; j++){ totalTorqueSum[i][j] = 0; }
			}
			nextTime += Param.output_torque_step;
		}
		totalTorqueSum[a.filamentIndex][a.index] += a.r * a.pf * Integrator.timestep;
		if(Integrator.timeElapsed/1000 > nextTime) {
			totalTorqueOut.write("step "+nextTime+"\n");
			int numFilament = filamentList.size();
			for(int i = 0; i < filamentList.size(); i++) { 
				int filLength = filamentList.elementAt(i).length;
				for(int j = 0; j < filLength; j++){ 
					totalTorqueOut.write(i+" "+j+" "+
						(totalTorqueSum[i][j]/(Param.output_torque_step*1000))+" "+
							(motorTorqueSum[i][j]/(Param.output_torque_step*1000))+
								"\n");
				}
			}
			totalTorqueSum = null;
			motorTorqueSum = null;
		}
	}
	
}










